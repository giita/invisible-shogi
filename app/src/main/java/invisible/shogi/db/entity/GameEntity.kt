/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import invisible.shogi.db.converter.EnumConverter
import invisible.shogi.db.converter.InstantConverter
import invisible.shogi.db.converter.PositionConverter
import invisible.shogi.db.converter.UuidConverter
import invisible.shogi.logic.Position
import invisible.shogi.model.Game
import invisible.shogi.model.GameMode
import invisible.shogi.model.GameResult
import invisible.shogi.model.Player
import org.joda.time.Instant
import java.util.*

@Entity
@TypeConverters(PositionConverter::class, InstantConverter::class, UuidConverter::class, EnumConverter::class)
class GameEntity: Game {
    @PrimaryKey(autoGenerate = true)
    override var id: Long = 0
    override var myUuid: UUID? = null
    override var remoteUuid: UUID? = null
    override var isArchived: Boolean = false
    override var comment: String? = null
    override var name: String? = null

    override var mode: GameMode = GameMode.LOCAL_PERSON // TODO: nullable?
    override var startingPosition // null means the default
            : Position? = null
    override var startedAt: Instant? = null
    override var lastActivityAt: Instant? = null

    override var ourPlayer: Player = Player.BLACK // TODO: nullable?

    override var result: GameResult? = null
    override var privateKeyId: Long = 0
    override var remoteDestination: String? = null

    var contactId: Int? = null
}