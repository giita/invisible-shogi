/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.db.entity

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import invisible.shogi.model.contact.Contact

@Entity
class ContactEntity(
    @PrimaryKey(autoGenerate = true)
    override var id: Long = 0,
    override var nickname: String = "",
    override var destination: String = ""): Contact {

    @Ignore
    constructor(contact: Contact) : this() {
        //this.id = contact.getId(); // should be done manually
        nickname = contact.nickname
        destination = contact.destination
    }
}