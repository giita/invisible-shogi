/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.db.entity

import androidx.room.*
import invisible.shogi.db.converter.CoordinatesConverter
import invisible.shogi.db.converter.EnumConverter
import invisible.shogi.db.converter.InstantConverter
import invisible.shogi.model.Coordinates
import invisible.shogi.model.Piece
import invisible.shogi.model.move.GameMove
import org.joda.time.Instant

@Entity(foreignKeys = [ForeignKey(
        entity = GameEntity::class,
        parentColumns = ["id"],
        childColumns = ["gameId"],
        onDelete = ForeignKey.CASCADE)], indices = [Index("gameId")])
@TypeConverters(InstantConverter::class, CoordinatesConverter::class, EnumConverter::class)
class GameMoveEntity : GameMove {
    @PrimaryKey(autoGenerate = true)
    override var id: Long = 0
    override var gameId: Long = 0
    override var numberFromStart: Int = 0
    override var instantMade: Instant? = null
    override var instantReceived: Instant? = null

    @Embedded
    override var piece: Piece
    override var pieceCoordinatesBefore // null means hand
            : Coordinates?
    override var pieceCoordinatesAfter: Coordinates
    override var isPromoting: Boolean

    constructor(piece: Piece, pieceCoordinatesBefore: Coordinates?, pieceCoordinatesAfter: Coordinates, promoting: Boolean) {
        this.piece = piece
        this.pieceCoordinatesBefore = pieceCoordinatesBefore
        this.pieceCoordinatesAfter = pieceCoordinatesAfter
        this.isPromoting = promoting
    }

    @Ignore
    constructor(gameMove: GameMove) {
        gameId = gameMove.gameId
        numberFromStart = gameMove.numberFromStart
        instantMade = gameMove.instantMade
        instantReceived = gameMove.instantReceived
        piece = gameMove.piece
        pieceCoordinatesBefore = gameMove.pieceCoordinatesBefore
        pieceCoordinatesAfter = gameMove.pieceCoordinatesAfter
        isPromoting = gameMove.isPromoting
    }
}