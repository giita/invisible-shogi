/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.db.entity

import androidx.room.DatabaseView
import androidx.room.TypeConverters
import invisible.shogi.db.converter.EnumConverter
import invisible.shogi.db.converter.InstantConverter
import invisible.shogi.model.GameMode
import invisible.shogi.model.Player
import org.joda.time.Instant

@DatabaseView("select GameEntity.id, name, startedAt, lastActivityAt, isArchived, ourPlayer, mode, count(GameMoveEntity.id) + 1 as currentMoveNumber" +
        " from GameEntity left join GameMoveEntity on GameEntity.id == GameMoveEntity.gameId group by GameEntity.id")
@TypeConverters(InstantConverter::class, EnumConverter::class)
data class GameInfo (
        var id: Long,
        var name: String,
        var startedAt: Instant,
        var lastActivityAt: Instant?,
        var isArchived: Boolean,
        var ourPlayer: Player,
        var mode: GameMode,
        var currentMoveNumber: Int) {
    val ourPlayerToMove: Boolean
        get() = (ourPlayer == Player.BLACK) xor (currentMoveNumber % 2 == 0)
}