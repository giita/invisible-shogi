/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.db.converter

import androidx.room.TypeConverter
import invisible.shogi.model.*

class EnumConverter {
    @TypeConverter
    fun intEnumToInt(enum: IntEnum?): Int? = enum?.code

    @TypeConverter
    fun pieceTypeFromInt(code: Int?): PieceType? = code?.let { fromCode(it) }

    @TypeConverter
    fun playerFromInt(code: Int?): Player? = code?.let { fromCode(it) }

    @TypeConverter
    fun gameModeFromInt(code: Int?): GameMode? = code?.let { fromCode(it) }

    @TypeConverter
    fun gameResultFromInt(code: Int?): GameResult? = code?.let { fromCode(it) }
}
