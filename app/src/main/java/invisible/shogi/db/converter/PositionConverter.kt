/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.db.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import invisible.shogi.logic.Position

class PositionConverter {
    @TypeConverter
    fun positionFromJson(json: String?): Position? =
            json?.let { Gson().fromJson(it, Position::class.java) }

    @TypeConverter
    fun positionToJson(position: Position?): String? =
            position?.let { Gson().toJson(it) }
}