/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.TypeConverters
import invisible.shogi.db.converter.EnumConverter
import invisible.shogi.db.converter.InstantConverter
import invisible.shogi.db.converter.UuidConverter
import invisible.shogi.db.entity.GameEntity
import invisible.shogi.db.entity.GameInfo
import invisible.shogi.db.entity.GameWithMoves
import invisible.shogi.model.GameMode
import java.util.*

@Dao
@TypeConverters(UuidConverter::class, InstantConverter::class, EnumConverter::class)
interface GameDao : BaseDao<GameEntity?> {
    @get:Query("select * from GameInfo where isArchived = 0")
    val liveOngoingGamesBriefs: LiveData<List<GameInfo>>

    @get:Query("select * from GameInfo where isArchived = 1")
    val liveArchivedGamesBriefs: LiveData<List<GameInfo>>

    @Query("select * from GameEntity where id = :id")
    suspend fun getGameById(id: Long): GameEntity

    @Query("select * from GameEntity where myUuid = :myUuid")
    suspend fun getGameByMyUuid(myUuid: UUID): GameEntity

    @Query("select * from GameEntity where mode = :mode")
    suspend fun getGamesByMode(mode: GameMode): List<GameEntity>

    @Transaction
    @Query("select * from GameEntity where id = :id")
    fun liveGameWithMovesById(id: Long): LiveData<GameWithMoves>
}