/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import invisible.shogi.db.dao.*
import invisible.shogi.db.entity.*

@Database(entities = [ContactEntity::class, GameEntity::class, GameMoveEntity::class, PrivateKeyEntity::class, MessageEntity::class], views = [GameInfo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract val gameDao: GameDao
    abstract val contactDao: ContactDao
    abstract val moveDao: MoveDao
    abstract val privateKeyDao: PrivateKeyDao
    abstract val messageDao: MessageDao

    companion object {
        private const val DATABASE_NAME = "database"

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        DATABASE_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}