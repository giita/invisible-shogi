/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.preference.PreferenceManager
import invisible.shogi.service.NetworkService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import net.danlew.android.joda.JodaTimeAndroid

class InvisibleShogiApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        appContext = this
        JodaTimeAndroid.init(this)
        ChannelRegistrar(this).registerChannels()
        if (PreferenceManager.getDefaultSharedPreferences(applicationContext).getBoolean("start_service_on_application_start", false)) {
            startService(Intent(this, NetworkService::class.java))
        }
    }

    companion object {
        lateinit var appContext: Context
            private set
        val appScope = CoroutineScope(SupervisorJob())
    }
}