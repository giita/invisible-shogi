/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.engines

import invisible.shogi.logic.Position
import invisible.shogi.model.Coordinates
import invisible.shogi.model.Piece
import invisible.shogi.model.PieceType
import invisible.shogi.model.PieceType.*
import invisible.shogi.model.Player
import invisible.shogi.model.move.Move
import invisible.shogi.model.move.MoveFactory.finishMove
import invisible.shogi.model.move.MoveFactory.startDrop
import invisible.shogi.model.move.MoveFactory.startMove
import java.util.*

fun PieceType.toUsiSfen(): String {
   return when (this) {
       KING -> "K"
       ROOK -> "R"
       PROMOTED_ROOK -> "+R"
       BISHOP -> "B"
       PROMOTED_BISHOP -> "+B"
       GOLD -> "G"
       SILVER -> "S"
       PROMOTED_SILVER -> "+S"
       KNIGHT -> "N"
       PROMOTED_KNIGHT -> "+N"
       LANCE -> "L"
       PROMOTED_LANCE -> "+L"
       PAWN -> "P"
       PROMOTED_PAWN -> "+P"
   }
}

fun pieceTypeFromUsiSfen(pieceUsi: String): PieceType {
    return when (pieceUsi) {
        "K" -> KING
        "R" -> ROOK
        "+R" -> PROMOTED_ROOK
        "B" -> BISHOP
        "+B" -> PROMOTED_BISHOP
        "G" -> GOLD
        "S" -> SILVER
        "+S" -> PROMOTED_SILVER
        "N" -> KNIGHT
        "+N" -> PROMOTED_KNIGHT
        "L" -> LANCE
        "+L" -> PROMOTED_LANCE
        "P" -> PAWN
        "+P" -> PROMOTED_PAWN
        else -> error("unknown piece")
    }
}

fun Piece.toUsiSfen(): String {
    val result = StringBuilder()
    var s: String = type.toUsiSfen()
    if (player == Player.WHITE) {
        s = s.toLowerCase(Locale.ROOT)
    }
    result.append(s)
    return result.toString()
}

fun handToUsi(h: List<Piece>): String {
    val types = listOf(ROOK, BISHOP, GOLD, SILVER, KNIGHT, LANCE, PAWN)
    val counts = mutableListOf(0, 0, 0, 0, 0, 0, 0)
    for (p in h) {
        ++counts[types.indexOf(p.type)]
    }
    val result = StringBuilder()
    for (i in types.indices) {
        val c = counts[i]
        if (c != 0) {
            if (c != 1) {
                result.append(c)
            }
            result.append(types[i].toUsiSfen())
        }
    }
    return result.toString()
}

fun Position.toUsiSfen(): String {
    var emptyStrike = 0
    val result = StringBuilder()
    for (i in 0 until Position.BOARD_SIZE) {
        for (j in 0 until Position.BOARD_SIZE) {
            val piece = getPieceAt(Coordinates(i, j))
            if (piece != null) {
                if (emptyStrike != 0) {
                    result.append(emptyStrike)
                    emptyStrike = 0
                }
                result.append(piece.toUsiSfen())
            } else {
                ++emptyStrike
            }
        }
        if (emptyStrike != 0) {
            result.append(emptyStrike)
            emptyStrike = 0
        }
        if (i != Position.BOARD_SIZE - 1) {
            result.append('/')
        }
    }
    result.append(' ')
    result.append(if (playerToMove == Player.BLACK) 'b' else 'w')
    result.append(' ')
    var h = handToUsi(getPiecesOnHand(Player.BLACK)) +
            handToUsi(getPiecesOnHand(Player.WHITE)).toLowerCase(Locale.ROOT)
    if (h == "") {
        h = "-"
    }
    result.append(h)
    result.append(' ')
    result.append(currentMoveNumber)
    return result.toString()
}

fun moveFromUsi(moveUsi: String, position: Position): Move {
    val promoting = moveUsi.length >= 5 // Fifth is '+' if it's there
    return if (moveUsi[1] == '*') { // Drop, example: P*3d
        val coord = Coordinates.fromWesternNotation(moveUsi.substring(2))
        finishMove(startDrop(Piece(position.playerToMove,
                pieceTypeFromUsiSfen(moveUsi.substring(0, 1)))),
                coord,
                promoting)
    } else {
        val before = Coordinates.fromWesternNotation(moveUsi.substring(0, 2))
        val after = Coordinates.fromWesternNotation(moveUsi.substring(2))
        finishMove(startMove(position.getPieceAt(before)!!, before),
                after,
                promoting)
    }
}
