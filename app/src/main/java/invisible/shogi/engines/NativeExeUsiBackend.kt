/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.engines

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.BufferedReader

abstract class NativeExeUsiBackend : UsiBackend {
    lateinit var process: Process
    private lateinit var reader: BufferedReader

    protected abstract fun libraryName(): String
    protected abstract fun checkIndicationString(s: String): Boolean

    override suspend fun init(context: Context) {
        val runtime = Runtime.getRuntime()
        process = withContext(Dispatchers.IO) {
            runtime.exec(arrayOf(context.applicationInfo.nativeLibraryDir + "/" + libraryName()))
        }
        reader = process
                .inputStream
                .bufferedReader()

        val info = readRawLine()
        check(checkIndicationString(info)) {
            "Bad engine indication string: $info"
        }
    }

    override suspend fun readRawLine(): String {
        return withContext(Dispatchers.IO) {
            reader.readLine()
        }
    }

    override suspend fun sendRawLine(s: String) {
        withContext(Dispatchers.IO) {
            val writer = process.outputStream.bufferedWriter()
            writer.write(s)
            writer.newLine()
            writer.flush()
        }
    }

    override fun destroy() {
        process.destroy()
    }
}