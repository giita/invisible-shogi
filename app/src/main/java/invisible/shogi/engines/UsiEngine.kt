/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.engines

import android.content.Context
import invisible.shogi.logic.Position
import invisible.shogi.model.move.Move
import org.joda.time.Duration

class UsiEngine(private val backend: UsiBackend) : Engine {
    var initialOptions: Map<String, String> = emptyMap()
    var moveTime = Duration.millis(200)

    private suspend fun sync() {
        backend.sendRawLine("isready")
        check(backend.readRawLine() == "readyok")
    }

    override suspend fun init(context: Context) {
        backend.init(context)

        backend.sendRawLine("usi")
        do {
            val line = backend.readRawLine()
        } while (line != "usiok")

        for (p in initialOptions) {
            setOption(p.key, p.value)
        }

        sync()
    }

    override suspend fun getMove(position: Position): Move {
        backend.sendRawLine("usinewgame")
        sync()
        backend.sendRawLine("position sfen ${position.toUsiSfen()}")
        backend.sendRawLine("go movetime ${moveTime.millis}")
        var line: String
        do {
            line = backend.readRawLine()
        } while (line.split(" ")[0] == "info")
        val bestmove = line.split(" ")
        check(bestmove[0] == "bestmove")
        return moveFromUsi(bestmove[1], position)
    }

    override fun destroy() {
        backend.destroy()
    }

    suspend fun setOption(key: String, value: String) {
        backend.sendRawLine("setoption name ${key} value ${value}")
    }
}
