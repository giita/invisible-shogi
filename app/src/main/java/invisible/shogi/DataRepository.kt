/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import invisible.shogi.InvisibleShogiApplication.Companion.appScope
import invisible.shogi.db.AppDatabase.Companion.getDatabase
import invisible.shogi.db.dao.*
import invisible.shogi.db.entity.*
import invisible.shogi.logic.IllegalMoveException
import invisible.shogi.logic.MoveChecker
import invisible.shogi.model.GameMode
import invisible.shogi.model.GameResult
import invisible.shogi.model.Player
import invisible.shogi.model.contact.Contact
import invisible.shogi.model.move.GameMove
import invisible.shogi.network.MessageSender
import invisible.shogi.network.MoveSender
import invisible.shogi.network.session.SessionManager.Companion.sessionManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.withContext
import net.i2p.data.Destination
import org.joda.time.Instant
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.ArrayList

class DataRepository(context: Context) {
    private val applicationContext = context.applicationContext
    private val db = getDatabase(applicationContext)
    private val gameDao: GameDao = db.gameDao
    private val moveDao: MoveDao = db.moveDao
    private val contactDao: ContactDao = db.contactDao
    private val privateKeyDao: PrivateKeyDao = db.privateKeyDao
    private val messageDao: MessageDao = db.messageDao
    private val sharedPreferences: SharedPreferences = applicationContext.getSharedPreferences("small_data", Context.MODE_PRIVATE)

    val moveAdditionLock = ReentrantLock()

    // Contacts
    suspend fun addContact(contact: Contact) {
        contactDao.insert(ContactEntity(contact))
    }

    suspend fun getContactById(contactId: Long): ContactEntity {
        return contactDao.getContactById(contactId)
    }

    suspend fun updateContact(contact: ContactEntity) {
        contactDao.update(contact)
    }

    // Moves
    suspend fun saveMove(move: GameMove) {
        check(moveAdditionLock.isHeldByCurrentThread) { "Move addition lock not held while saving move" }
        val game = getGameById(move.gameId)
        val moves = getMovesByGameId(move.gameId)
        val position = withContext(Dispatchers.Default) {
            try {
                game.realStartingPosition
                        .applyInplace(moves)
                        .applyInplace(move)
            } catch (e: IllegalMoveException) {
                throw IllegalArgumentException("Illegal move")
            }
        }
        moveDao.insert(GameMoveEntity(move))
        if (withContext(Dispatchers.Default) {MoveChecker.isCheckmate(position)}) {
            game.result = if (position.playerToMove == Player.WHITE) GameResult.GOTE_WON else GameResult.SENTE_WON
            gameDao.update(game)
        }
    }

    enum class NetworkedActionResult {
        SUCCESS, FAIL_CONTACT_NOT_FOUND, FAIL_COULD_NOT_SEND_SERVICE_NOT_FOUND, FAIL_COULD_NOT_SEND_OPPONENT_OFFLINE, FAIL_ILLEGAL_MOVE, FAIL_OPPONENT_DID_NOT_LIKE, FAIL_SESSION_NOT_UP
    }

    suspend fun makeMove(move: GameMove): NetworkedActionResult {
        moveAdditionLock.lock()
        return try {
            val game = getGameById(move.gameId)
            val moves = getMovesByGameId(move.gameId)
            try {
                game.realStartingPosition
                        .applyInplace(moves)
                        .applyInplace(move)
            } catch (e: IllegalMoveException) {
                return NetworkedActionResult.FAIL_ILLEGAL_MOVE
            }
            if (game.mode == GameMode.I2P_DIRECT) {
                val manager = sessionManager.getManagerForKeyId(game.privateKeyId)
                        ?: return NetworkedActionResult.FAIL_SESSION_NOT_UP
                try {
                    withContext(appScope.coroutineContext + IO) {
                        MoveSender(move, applicationContext).handle(manager.connect(Destination(game.remoteDestination)))
                    }
                } catch (e: Exception) {
                    return NetworkedActionResult.FAIL_COULD_NOT_SEND_OPPONENT_OFFLINE
                }
            } else if (game.mode == GameMode.I2P_RANDOM_MATCH_SERVER) {
                throw AssertionError("Not implemented")
            }
            // nothing to do if LOCAL
            saveMove(GameMoveEntity(move))
            NetworkedActionResult.SUCCESS
        } finally {
            moveAdditionLock.unlock()
        }
    }

    suspend fun getMovesByGameId(gameId: Long): List<GameMove> {
        return moveDao.getMovesByGameId(gameId)
    }

    fun liveGameWithMovesById(id: Long): LiveData<GameWithMoves> {
        return gameDao.liveGameWithMovesById(id).map {
            GameWithMoves(it.game, it.moves.sortedBy { it.numberFromStart })
        }
    }

    // Games
    suspend fun getGameByMyUuid(uuid: UUID): GameEntity {
        return gameDao.getGameByMyUuid(uuid)
    }

    suspend fun getGameById(gameId: Long): GameEntity {
        return gameDao.getGameById(gameId)
    }

    val contactsLive: LiveData<List<Contact>>
        get() = contactDao.liveContacts.map { ArrayList(it) }

    suspend fun addGame(game: GameEntity): Long {
        return gameDao.insert(game)
    }

    suspend fun getGamesByMode(mode: GameMode): List<GameEntity> {
        return gameDao.getGamesByMode(mode)
    }

    suspend fun deleteGame(gameId: Long) {
        val gameEntity = GameEntity().apply {
            id = gameId
        }
        gameDao.delete(gameEntity)
    }

    val liveOngoingGamesBriefs: LiveData<List<GameInfo>>
        get() = gameDao.liveOngoingGamesBriefs
    val liveArchivedGamesBriefs: LiveData<List<GameInfo>>
        get() = gameDao.liveArchivedGamesBriefs

    suspend fun archiveGame(gameId: Long) {
        val game = gameDao.getGameById(gameId)
        game.isArchived = true
        gameDao.update(game)
    }

    suspend fun updateGame(game: GameEntity) {
        gameDao.update(game)
    }

    suspend fun newLocalGame(mode: GameMode): Long {
        check(mode == GameMode.LOCAL_PERSON || mode == GameMode.LOCAL_ENGINE)
        val game = GameEntity().apply {
            this.mode = mode
            name = getNextGameName()
            isArchived = false
            startedAt = Instant.now()
        }
        return gameDao.insert(game)
    }

    // Messages
    fun liveMessagesByContactId(contactId: Long): LiveData<List<MessageEntity>> {
        return messageDao.liveMessagesByContactId(contactId)
    }

    fun liveMessagesByGameId(gameId: Long): LiveData<List<MessageEntity>> {
        return messageDao.liveMessagesByGameId(gameId)
    }

    suspend fun getContactByDestination(destination: String): ContactEntity {
        return contactDao.getContactByDestination(destination)
    }

    suspend fun addMessage(message: MessageEntity) {
        messageDao.insert(message)
    }

    suspend fun sendMessage(message: MessageEntity): NetworkedActionResult {
        val privateKeyId: Long
        val remoteDestination: String
        val gameId = message.gameId
        if (gameId != null) {
            val game = getGameById(gameId)
            val mode = game.mode
            if (mode == GameMode.LOCAL_PERSON) {
                addMessage(message)
                return NetworkedActionResult.SUCCESS
            }
            privateKeyId = game.privateKeyId
            remoteDestination = game.remoteDestination!!
        } else {
            if (BuildConfig.DEBUG && message.contactId == null) {
                error("Assertion failed")
            }
            privateKeyId = primaryKeyId
            val contact = getContactById(message.contactId!!)
            remoteDestination = contact.destination
        }
        val manager = sessionManager.getManagerForKeyId(privateKeyId)
                ?: return NetworkedActionResult.FAIL_SESSION_NOT_UP
        try {
            withContext(IO) { MessageSender(message, applicationContext).handle(manager.connect(Destination(remoteDestination))) }
        } catch (e: Exception) {
            return NetworkedActionResult.FAIL_COULD_NOT_SEND_OPPONENT_OFFLINE
        }
        addMessage(message)
        return NetworkedActionResult.SUCCESS
    }

    suspend fun deleteContact(contactId: Long) {
        val contactEntity = ContactEntity().apply {
            id = contactId
        }
        contactDao.delete(contactEntity)
    }

    // Miscellaneous

    suspend fun getNextGameNumber(): Int = withContext(Main) {
        val result = sharedPreferences.getInt("last_game_number", 0) + 1
        sharedPreferences.edit().putInt("last_game_number", result).apply()
        result
    }

    suspend fun getNextGameName(): String {
        return "Game #${getNextGameNumber()}"
    }

    suspend fun getPrivateKey(keyId: Long): ByteArray {
        return privateKeyDao.getPrivateKeyById(keyId).privateKey
    }

    suspend fun savePrivateKey(privateKey: ByteArray): Long {
        return privateKeyDao.insert(PrivateKeyEntity(privateKey))
    }

    val primaryKeyId: Long
        get() {
            val sharedPreferences = applicationContext.getSharedPreferences("key", Context.MODE_PRIVATE)
            return sharedPreferences.getLong("keyId", -1)
        }
}