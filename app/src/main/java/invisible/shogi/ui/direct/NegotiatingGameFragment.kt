/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.direct

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import invisible.shogi.R
import invisible.shogi.databinding.FragmentNegotiatingGameBinding
import invisible.shogi.network.session.SessionManager.Companion.sessionManager
import invisible.shogi.ui.game.GameActivity

class NegotiatingGameFragment: Fragment() {
    private val viewModel by activityViewModels<DirectConnectionViewModel>()
    private lateinit var binding: FragmentNegotiatingGameBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentNegotiatingGameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.gameNegotiationStatus.observe(viewLifecycleOwner) {
            if (it.isSuccess) {
                sessionManager.startSessions()
                val gameId = it.getOrThrow()
                val intent = Intent(requireContext(), GameActivity::class.java)
                intent.putExtra(GameActivity.EXTRA_GAME_ID, gameId)
                startActivity(intent)
            } else {
                Toast.makeText(requireActivity(), "Game negotiation error", Toast.LENGTH_LONG).show()
                findNavController().navigate(R.id.directConnectionFragment)
                return@observe
            }
            requireActivity().finish()
        }
    }
}