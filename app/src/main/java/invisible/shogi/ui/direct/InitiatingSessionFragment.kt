/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.direct

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import invisible.shogi.R
import invisible.shogi.databinding.FragmentInitiatingSessionBinding
import invisible.shogi.network.session.SessionInfo
import invisible.shogi.network.session.SessionInfo.Companion.CONNECTION_FAILED
import invisible.shogi.network.session.SessionInfo.Companion.CONNECTION_FINISHED

class InitiatingSessionFragment : Fragment() {
    private val viewModel by activityViewModels<DirectConnectionViewModel>()
    private lateinit var binding: FragmentInitiatingSessionBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentInitiatingSessionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.sessionInfo.observe(viewLifecycleOwner) { info: SessionInfo ->
            if (info.status == CONNECTION_FINISHED) {
                findNavController().navigate(R.id.action_initiatingSessionFragment_to_directConnectionFragment)
            }
            if (info.status == CONNECTION_FAILED) {
                Toast.makeText(requireActivity(), "Failed connecting session", Toast.LENGTH_LONG).show()
                requireActivity().finish()
            }
            binding.textView.text = info.toString()
        }
    }
}