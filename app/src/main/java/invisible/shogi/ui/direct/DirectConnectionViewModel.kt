/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.direct

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import invisible.shogi.DataRepository
import invisible.shogi.InvisibleShogiApplication
import invisible.shogi.network.ConnectionReceiver
import invisible.shogi.network.GameStartSender
import invisible.shogi.network.session.SessionInfo
import invisible.shogi.network.session.SessionManager.Companion.sessionManager
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.i2p.data.Destination

class DirectConnectionViewModel(application: Application) : AndroidViewModel(application) {
    val dataRepository = DataRepository(application)
    lateinit var sessionInfo: LiveData<SessionInfo>
    var sessionId: Long = -1
    private var requestedSession: Boolean = false
    lateinit var gameNegotiationStatus: MutableLiveData<Result<Long>>

    fun ensureRequestedSession() {
        if (!requestedSession) {
            sessionId = sessionManager.newSession("Direct connection session", ConnectionReceiver(InvisibleShogiApplication.appContext))
            sessionInfo = sessionManager.getSessionInfoLive(sessionId)
            requestedSession = true
        }
    }

    fun startGame(destination: String) {
        gameNegotiationStatus = MutableLiveData()
        // TODO: move to repository
        InvisibleShogiApplication.appScope.launch(IO) {
            val manager = withContext(Main) {
                sessionManager.getManager(sessionId)!!
            }
            (GameStartSender(InvisibleShogiApplication.appContext, gameNegotiationStatus)).handle(
                    manager.connect(Destination(destination))
            )
        }
    }

    override fun onCleared() {
        if (sessionId != -1L) {
            sessionManager.removeSession(sessionId)
        }
        super.onCleared()
    }
}