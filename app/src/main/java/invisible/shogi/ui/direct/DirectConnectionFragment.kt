/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.direct

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import invisible.shogi.R
import invisible.shogi.databinding.FragmentDirectConnectionBinding
import invisible.shogi.ui.QrCodeScanningActivity
import invisible.shogi.ui.QrUtil

class DirectConnectionFragment: Fragment() {
    private val viewModel by activityViewModels<DirectConnectionViewModel>()
    private lateinit var binding: FragmentDirectConnectionBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDirectConnectionBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun scanQR() {
        val intent = Intent(requireActivity(), QrCodeScanningActivity::class.java)
        startActivityForResult(intent, QR_CODE_RESULT_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == QR_CODE_RESULT_REQUEST) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                binding.inputDestination.setText(data!!.getStringExtra(QrCodeScanningActivity.EXTRA_SCANNING_RESULT))
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.scanQrButton.setOnClickListener {
            scanQR()
        }
        val info = viewModel.sessionInfo.value!!
        val myDestination = info.manager!!.session.myDestination.toBase64()
        binding.myDestinationQr.setImageDrawable(QrUtil.drawableWithoutSmoothing(resources, QrUtil.toQrBitmap(myDestination)))
        binding.myDestinationQr.setOnClickListener {
            val clipboard = requireActivity().getSystemService(CLIPBOARD_SERVICE) as ClipboardManager?
            clipboard?.setPrimaryClip(ClipData.newPlainText("Local destination QR", myDestination))
            Toast.makeText(requireActivity(), "Copied local destination to clipboard", Toast.LENGTH_SHORT).show()
        }
        binding.connect.setOnClickListener {
            if (binding.inputDestination.text == null) {
                return@setOnClickListener
            }
            val destination = binding.inputDestination.text.toString()
            findNavController().navigate(R.id.action_directConnectionFragment_to_negotiatingGameFragment)
            viewModel.startGame(destination)
        }
    }

    companion object {
        const val QR_CODE_RESULT_REQUEST = 0
    }
}