/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.game.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.gridlayout.widget.GridLayout
import invisible.shogi.R
import invisible.shogi.logic.MoveChecker.Companion.noPossibleMoves
import invisible.shogi.logic.Position
import invisible.shogi.logic.PossibleMoves
import invisible.shogi.model.Coordinates
import invisible.shogi.model.Player

/** The main board view.  */
class BoardView(context: Context?, attrs: AttributeSet?) : GridLayout(context, attrs) {
    private var playerBelow = Player.BLACK
    private var selectedTile: Coordinates? = null
    private var end: Coordinates? = null
    private var possibleMoves: PossibleMoves = noPossibleMoves()
    private var onTileClickListener: OnTileClickListener? = null
    private val tileViews = Array(Position.BOARD_SIZE) { arrayOfNulls<View>(Position.BOARD_SIZE) }
    private var position: Position? = null
    fun updatePosition(position: Position?) {
        this.position = position
        updateAllTiles()
    }

    fun selectTile(coordinates: Coordinates?) {
        val previousSelectedTile = selectedTile
        selectedTile = coordinates
        updateTile(previousSelectedTile)
        updateTile(selectedTile)
    }

    fun setPossibleMoves(possibleMoves: PossibleMoves) {
        this.possibleMoves = possibleMoves
        updateAllTiles()
    }

    private fun updateAllTiles() {
        for (i in 0 until Position.BOARD_SIZE) {
            for (j in 0 until Position.BOARD_SIZE) {
                updateTile(i, j)
            }
        }
    }

    private fun updateTile(coordinates: Coordinates?) {
        if (coordinates == null) {
            return
        }
        updateTile(coordinates.row, coordinates.column)
    }

    private fun updateTile(row: Int, column: Int) {
        val tileView = tileViews[row][column]
        val tileImageView = tileView!!.findViewById<ImageView>(R.id.piece)
        tileView.setBackgroundResource(R.drawable.tile_unselected_background)
        var canWithoutPromoting = false
        var canPromoting = false
        for (move in possibleMoves[row][column]) {
            if (move.isPromoting) {
                canPromoting = true
            } else {
                canWithoutPromoting = true
            }
        }
        if (canWithoutPromoting && canPromoting) {
            tileView.setBackgroundResource(R.drawable.tile_can_promote_or_not_background)
        } else if (canWithoutPromoting) {
            tileView.setBackgroundResource(R.drawable.tile_can_without_promoting_background)
        } else if (canPromoting) {
            tileView.setBackgroundResource(R.drawable.tile_can_promoting_background)
        }
        val coordinates = Coordinates(row, column)
        if (coordinates == selectedTile) {
            tileView.setBackgroundResource(R.drawable.tile_selected_background)
        } else if (coordinates == end) {
            tileView.setBackgroundResource(R.drawable.tile_move_ending_background)
        }
        if (position != null) {
            val p = position!!.getPieceAt(Coordinates(row, column))
            if (p != null) {
                tileImageView.setImageResource(p.resourceId)
                if (p.player != playerBelow) {
                    tileImageView.rotation = 180f
                } else {
                    tileImageView.rotation = 0f
                }
            } else {
                tileImageView.setImageResource(0)
            }
        }
    }

    private fun mirrorTiles() {
        Util.rotate2DArray(tileViews)
        for (i in 0 until Position.BOARD_SIZE) {
            for (j in 0 until Position.BOARD_SIZE) {
                tileViews[i][j]!!.setOnClickListener { u: View? -> onTileClickListener!!.onTileClick(u, Coordinates(i, j)) }
            }
        }
    }

    fun setPlayerBelow(playerBelow: Player) {
        if (playerBelow != this.playerBelow) {
            mirrorTiles()
        }
        this.playerBelow = playerBelow
        updateAllTiles()
    }

    fun setOnTileClickListener(onTileClickListener: OnTileClickListener?) {
        this.onTileClickListener = onTileClickListener
    }

    fun setMoveEnd(end: Coordinates?) {
        this.end = end
        updateAllTiles()
    }

    fun interface OnTileClickListener {
        fun onTileClick(v: View?, coordinates: Coordinates)
    }

    init {
        rowCount = Position.BOARD_SIZE
        columnCount = Position.BOARD_SIZE
        alignmentMode = ALIGN_BOUNDS
        val layoutInflater = LayoutInflater.from(context)
        for (i in 0 until Position.BOARD_SIZE) {
            for (j in 0 until Position.BOARD_SIZE) {
                val tileLayout = layoutInflater.inflate(R.layout.board_tile, this, false)
                tileLayout.setOnClickListener { u: View? -> onTileClickListener!!.onTileClick(u, Coordinates(i, j)) }
                tileViews[i][j] = tileLayout
                val layoutParams = LayoutParams(spec(i, FILL, 1f), spec(j, FILL, 1f))
                layoutParams.width = 0
                layoutParams.height = 0
                addView(tileLayout, layoutParams)
            }
        }
        updateAllTiles()
    }
}