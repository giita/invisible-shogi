/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.game.view


object Util {
    fun <T> rotate2DArray(array: Array<Array<T>>) {
        val height = array.size
        if (height == 0) {
            return
        }
        val width: Int = array[0].size
        for (i in 0 until height) {
            for (j in 0 until width) {
                val mirroredI = height - i - 1
                val mirroredJ = width - j - 1
                if (mirroredI < i || mirroredI == i && mirroredJ <= j) {
                    return
                }
                val tmp = array[i][j]
                array[i][j] = array[mirroredI][mirroredJ]
                array[mirroredI][mirroredJ] = tmp
            }
        }
    }
}