/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.game

import android.app.Application
import androidx.lifecycle.*
import invisible.shogi.DataRepository
import invisible.shogi.DataRepository.NetworkedActionResult
import invisible.shogi.InvisibleShogiApplication.Companion.appScope
import invisible.shogi.db.entity.GameWithMoves
import invisible.shogi.engines.Engine
import invisible.shogi.engines.EngineFactory.fairyStockfish
import invisible.shogi.logic.MoveChecker
import invisible.shogi.logic.MoveChecker.Companion.noPossibleMoves
import invisible.shogi.logic.Position
import invisible.shogi.logic.PossibleMoves
import invisible.shogi.model.*
import invisible.shogi.model.move.Move
import invisible.shogi.model.move.MoveFactory.finishMove
import invisible.shogi.model.move.MoveFactory.makeMoveIn
import invisible.shogi.model.move.MoveFactory.startDrop
import invisible.shogi.model.move.MoveFactory.startMove
import invisible.shogi.model.move.StartedMove
import invisible.shogi.ui.livedatautil.Event
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.joda.time.Duration
import org.joda.time.Instant

enum class RelativePlayer {
    BOTTOM,
    TOP
}

class GameViewModel(application: Application, private val gameId: Long) : ViewModel() {
    private val app = application
    private val dataRepository: DataRepository = DataRepository(application)

    private val lastPosition: LiveData<Position>
    val viewedPosition: LiveData<Position>
    var possibleMoves: LiveData<out PossibleMoves>
    var playerToMove: LiveData<RelativePlayer>
    val gameWithMoves: LiveData<GameWithMoves>

    // Local state

    private val _selectedTile = MutableLiveData<Coordinates?>(null)
    val selectedTile: LiveData<Coordinates?>
        get() = _selectedTile

    private val startedMove = MutableLiveData<StartedMove?>(null)

    private val _selectedMoveEnd = MutableLiveData<Coordinates?>(null)
    val selectedMoveEnd: LiveData<Coordinates?>
        get() = _selectedMoveEnd

    private val _bottomPlayerSelectedCapturedPieceType = MutableLiveData<PieceType?>(null)
    val bottomPlayerSelectedCapturedPieceType: LiveData<PieceType?>
        get() = _bottomPlayerSelectedCapturedPieceType

    private val _topPlayerSelectedCapturedPieceType = MutableLiveData<PieceType?>(null)
    val topPlayerSelectedCapturedPieceType: LiveData<PieceType?>
        get() = _topPlayerSelectedCapturedPieceType

    private var selectedCapturedPieceType: PieceType? = null
    private val historyCurrentMove = MutableLiveData<Int>(null)
    private val _watchingHistory = MutableLiveData(false)
    private val watchingHistory: LiveData<Boolean>
        get() = _watchingHistory

    private val _showProgressBar = MutableLiveData(false)
    val showProgressBar: LiveData<Boolean>
        get() = _showProgressBar

    private val _toast = MutableLiveData<Event<String>?>(null)
    val toast: LiveData<Event<String>?>
        get() = _toast

    private val _showMakePromotingMoveButton = MutableLiveData(false)
    val showMakePromotingMoveButton: LiveData<Boolean>
        get() = _showMakePromotingMoveButton

    private val _showMakeRegularMoveButton = MutableLiveData(false)
    val showMakeRegularMoveButton: LiveData<Boolean>
        get() = _showMakeRegularMoveButton

    lateinit var engine: Engine

    init {
        gameWithMoves = dataRepository.liveGameWithMovesById(gameId)

        lastPosition = gameWithMoves.switchMap {
            liveData(viewModelScope.coroutineContext) {
                emit(Position().applyInplace(it.moves))
            }
        }

        viewedPosition = watchingHistory.switchMap {
            if (it) {
                historyCurrentMove.switchMap { histMove ->
                    gameWithMoves.switchMap {
                        liveData(viewModelScope.coroutineContext) {
                            emit(Position().applyInplace(it.moves.subList(0, histMove)))
                        }
                    }
                }
            } else {
                lastPosition
            }
        }

        playerToMove = viewedPosition.switchMap { position ->
            gameWithMoves.map {
                if (position.playerToMove == it.game.ourPlayer) {
                    RelativePlayer.BOTTOM
                } else {
                    RelativePlayer.TOP
                }
            }
        }

        possibleMoves = startedMove.switchMap { move ->
            if (move == null) {
                liveData { emit(noPossibleMoves() as PossibleMoves) }
            } else {
                viewedPosition.switchMap {
                    liveData {
                        emit(MoveChecker.getPossibleMoveEndingsByCoordinatesSuspend(it, move))
                    }
                }
            }
        }

        gameWithMoves.observeForever(object : Observer<GameWithMoves> {
            override fun onChanged(gm: GameWithMoves) {
                if (gm.game.mode != GameMode.LOCAL_ENGINE) {
                    return
                }
                val t = this
                viewModelScope.launch {
                    val position = Position().applyInplace(gm.moves)
                    if (position.playerToMove != gm.game.ourPlayer) {
                        launchEngine(position)
                    }
                    withContext(Main) {
                        gameWithMoves.removeObserver(t)
                    }
                }
            }
        })
    }

    override fun onCleared() {
        if (this::engine.isInitialized) {
            engine.destroy()
        }
        super.onCleared()
    }

    fun launchEngine(position: Position) {
        viewModelScope.launch {
            if (!this@GameViewModel::engine.isInitialized) {
                engine = fairyStockfish(skillLevel = 4, moveTime = Duration.millis(100))
                engine.init(app)
            }
            val move = engine.getMove(position)
            withContext(Main) {
                dataRepository.moveAdditionLock.lock()
                dataRepository.saveMove(makeMoveIn(move, gameId, position.currentMoveNumber, Instant.now(), null))
                dataRepository.moveAdditionLock.unlock()
            }
        }
    }

    fun boardTileClicked(game: Game, position: Position, coordinates: Coordinates) {
        viewModelScope.launch {
            if (selectedEnd() && _selectedMoveEnd.value == coordinates) {
                deselectEnd()
                return@launch
            }
            if (selectedStart()) {
                if (coordinates == _selectedTile.value) {
                    deselect()
                    return@launch
                }
                if (position.getPieceAt(coordinates)?.player == position.playerToMove) {
                    deselect()
                } else {
                    var found = false
                    deselectEnd()
                    for (toPromote in listOf(false, true)) {
                        if (MoveChecker.isLegalMoveSuspend(position, finishMove(startedMove.value!!, coordinates, toPromote))) {
                            if (toPromote) {
                                _showMakePromotingMoveButton.value = true
                            } else {
                                _showMakeRegularMoveButton.value = true
                            }
                            found = true
                        }
                    }
                    if (found) {
                        _selectedMoveEnd.value = coordinates
                    } else {
                        deselect()
                    }
                    return@launch
                }
            }
            if (canNotSelect(game)
                    || watchingHistory.value!! || position.getPieceAt(coordinates) == null || position.getPieceAt(coordinates)!!.player != position.playerToMove) {
                return@launch
            }
            deselect()
            _selectedTile.value = coordinates
            startedMove.value = startMove(position.getPieceAt(_selectedTile.value!!)!!, _selectedTile.value!!)
        }
    }

    fun makeMove(game: Game, position: Position, withPromotion: Boolean) {
        recordMove(position, finishMove(startedMove.value!!, _selectedMoveEnd.value!!, withPromotion))
        deselect()
        // TODO: some repetition with init
        if (game.mode == GameMode.LOCAL_ENGINE) {
            gameWithMoves.observeForever(object : Observer<GameWithMoves> {
                override fun onChanged(gm: GameWithMoves) {
                    val t = this
                    viewModelScope.launch {
                        val newPosition = Position().applyInplace(gm.moves)
                        if (newPosition.playerToMove != gm.game.ourPlayer) {
                            launchEngine(newPosition)
                            withContext(Main) {
                                gameWithMoves.removeObserver(t)
                            }
                        }
                    }
                }
            })
        }
    }

    private fun recordMove(position: Position, move: Move) {
        _showProgressBar.value = true
        viewModelScope.launch(Main) {
            val result = dataRepository.makeMove(makeMoveIn(move, gameId, position.currentMoveNumber, Instant.now(), null))
            _showProgressBar.value = false
            when (result) {
                NetworkedActionResult.SUCCESS -> {
                }
                NetworkedActionResult.FAIL_ILLEGAL_MOVE,
                NetworkedActionResult.FAIL_OPPONENT_DID_NOT_LIKE,
                NetworkedActionResult.FAIL_COULD_NOT_SEND_OPPONENT_OFFLINE ->
                    _toast.setValue(Event("Could not send the move"))
                else -> _toast.setValue(Event("Could not send the move"))
            }
        }
    }

    fun selectedEnd(): Boolean {
        return _selectedMoveEnd.value != null
    }

    fun selectedStart(): Boolean {
        return _selectedTile.value != null || selectedCapturedPieceType != null
    }

    fun deselect() {
        startedMove.value = null
        _selectedTile.value = null
        setSelectedCapturedPieceType(null)
        deselectEnd()
    }

    fun deselectEnd() {
        _selectedMoveEnd.value = null
        _showMakePromotingMoveButton.value = false
        _showMakeRegularMoveButton.value = false
    }

    private fun topMove(): Boolean {
        return playerToMove.value == RelativePlayer.TOP
    }

    private fun bottomMove(): Boolean {
        return playerToMove.value == RelativePlayer.BOTTOM
    }

    private fun canNotSelect(game: Game): Boolean {
        return !canMove(game) || game.mode != GameMode.LOCAL_PERSON && !bottomMove() || _showProgressBar.value!!
    }

    private fun canMove(game: Game): Boolean {
        return !game.isArchived
    }

    fun playerCapturedPieceSelected(game: Game, position: Position, relativePlayer: RelativePlayer, pieceType: PieceType?) {
        if (canNotSelect(game)) {
            return
        }
        if (relativePlayer != playerToMove.value!! ||
                pieceType == null ||
                pieceType == selectedCapturedPieceType) {
            deselect()
            return
        }
        deselect()
        setSelectedCapturedPieceType(pieceType)
        startedMove.value = startDrop(Piece(position.playerToMove, pieceType))
    }

    private fun setSelectedCapturedPieceType(pieceType: PieceType?) {
        _bottomPlayerSelectedCapturedPieceType.value = if (pieceType == null || !bottomMove()) null else pieceType
        _topPlayerSelectedCapturedPieceType.value = if (pieceType == null || !topMove()) null else pieceType
        selectedCapturedPieceType = pieceType
    }

    fun setViewedMove(moveNumber: Int) {
        if (moveNumber == -1) {
            _watchingHistory.value = false
            historyCurrentMove.value = null
        } else {
            historyCurrentMove.value = moveNumber
            _watchingHistory.value = true
        }
        deselect()
    }

    private fun archive() {
        appScope.launch {
            dataRepository.archiveGame(gameId)
        }
    }
}