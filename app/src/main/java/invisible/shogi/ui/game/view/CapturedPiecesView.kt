/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.game.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.gridlayout.widget.GridLayout
import invisible.shogi.R
import invisible.shogi.logic.Position
import invisible.shogi.model.Piece
import invisible.shogi.model.PieceType
import invisible.shogi.model.Player

class CapturedPiecesView(context: Context, attrs: AttributeSet?) : GridLayout(context, attrs) {
    private var playerBelow = Player.BLACK
    private val ourPlayer: Player
        get() = if (forUs) playerBelow else playerBelow.oppositePlayer
    private var forUs = false
    private var vertical = false

    private var selectedPieceType: PieceType? = null
    private var onPieceClickListener: OnPieceClickListener? = null
    private var heightPieces = 0
    private var widthPieces = 0
    private val tileViews: Array<Array<View?>>
    private var position: Position? = null

    private val pieceAt: Array<Array<PieceType?>>
    private fun mirrorTiles() {
        Util.rotate2DArray(pieceAt)
    }

    fun updatePosition(position: Position?) {
        this.position = position
        updateAllTiles()
    }

    fun selectPiece(pieceType: PieceType?) {
        selectedPieceType = pieceType
        updateAllTiles()
    }

    private fun updateAllTiles() {
        for (i in 0 until heightPieces) {
            for (j in 0 until widthPieces) {
                updateTile(i, j)
            }
        }
    }

    private fun updateTile(row: Int, column: Int) {
        val tileView = tileViews[row][column]
        val tileImageView = tileView!!.findViewById<ImageView>(R.id.piece)
        val countText = tileView.findViewById<TextView>(R.id.count)
        tileView.setBackgroundResource(R.drawable.captured_piece_unselected_background)
        if (position == null || pieceAt[row][column] == null) {
            return
        }
        var count = 0
        for (a in position!!.getPiecesOnHand(ourPlayer)) {
            if (a.type == pieceAt[row][column]) {
                ++count
            }
        }
        if (count == 0) {
            tileImageView.setImageResource(0)
        } else {
            // King can't be captured so player should not matter here
            tileImageView.setImageResource(Piece(Player.BLACK, pieceAt[row][column]!!).resourceId)
            if (forUs) {
                tileImageView.rotation = 0f
            } else {
                tileImageView.rotation = 180f
            }
        }
        if (count > 1) {
            countText.visibility = VISIBLE
            countText.text = count.toString()
        } else {
            countText.visibility = GONE
            countText.text = null
        }
        if (pieceAt[row][column] == selectedPieceType) {
            tileView.setBackgroundResource(R.drawable.captured_piece_selected_background)
        }
    }

    fun setPlayerBelow(playerBelow: Player) {
        this.playerBelow = playerBelow
        updateAllTiles()
    }

    fun setOnTileClickListener(onTileClickListener: OnPieceClickListener?) {
        onPieceClickListener = onTileClickListener
    }

    fun interface OnPieceClickListener {
        fun onPieceClick(v: View?, pieceType: PieceType?)
    }

    init {
        alignmentMode = ALIGN_BOUNDS
        val a = context.obtainStyledAttributes(attrs, R.styleable.CapturedPiecesView)
        try {
            forUs = a.getBoolean(R.styleable.CapturedPiecesView_forPlayerBelow, false)
            vertical = a.getInteger(R.styleable.CapturedPiecesView_capturedPiecesBoardOrientation, 0) == 1
        } finally {
            a.recycle()
        }
        if (vertical) {
            heightPieces = 4
            widthPieces = 2
            pieceAt = arrayOf(arrayOf(PieceType.PAWN, PieceType.SILVER), arrayOf(PieceType.ROOK, PieceType.BISHOP), arrayOf(PieceType.GOLD, PieceType.LANCE), arrayOf(null, PieceType.KNIGHT))
        } else {
            heightPieces = 2
            widthPieces = 4
            pieceAt = arrayOf(arrayOf(PieceType.PAWN, PieceType.SILVER, PieceType.ROOK, PieceType.BISHOP), arrayOf(null, PieceType.KNIGHT, PieceType.GOLD, PieceType.LANCE))
        }
        rowCount = heightPieces
        columnCount = widthPieces
        tileViews = Array(heightPieces) { arrayOfNulls(widthPieces) }
        val layoutInflater = LayoutInflater.from(context)
        if (!forUs) {
            mirrorTiles()
        }
        for (i in 0 until heightPieces) {
            for (j in 0 until widthPieces) {
                val tileLayout = layoutInflater.inflate(R.layout.captured_piece_tile, this, false)
                tileLayout.setOnClickListener { u: View? ->
                    if (onPieceClickListener == null) {
                        return@setOnClickListener
                    }
                    val found = pieceAt[i][j] in
                            position!!.getPiecesOnHand(ourPlayer).map { it.type }
                    onPieceClickListener!!.onPieceClick(u, if (found) { pieceAt[i][j] } else { null })
                }
                tileViews[i][j] = tileLayout
                val layoutParams = LayoutParams(spec(i, FILL, 1f), spec(j, FILL, 1f))
                layoutParams.width = 0
                layoutParams.height = 0
                tileLayout.setBackgroundResource(R.drawable.captured_piece_unselected_background)
                tileLayout.findViewById<View>(R.id.count).visibility = GONE
                addView(tileLayout, layoutParams)
            }
        }
        updateAllTiles()
    }
}