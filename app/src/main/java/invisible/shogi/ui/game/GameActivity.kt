/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.game

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import invisible.shogi.R
import invisible.shogi.databinding.ActivityGameBinding
import invisible.shogi.db.entity.GameWithMoves
import invisible.shogi.logic.Position
import invisible.shogi.model.Coordinates
import invisible.shogi.model.Game
import invisible.shogi.model.PieceType
import invisible.shogi.model.move.GameMove
import invisible.shogi.ui.chat.ChatActivity
import invisible.shogi.ui.dialog.GameInfoDialogFragment.Companion.newInstance
import invisible.shogi.ui.game.MoveListAdapter.ListInteractionListener

class GameActivity : AppCompatActivity(), ListInteractionListener {
    private lateinit var binding: ActivityGameBinding
    private val viewModel by viewModels<GameViewModel> {
        val intent = intent
        gameId = intent.getLongExtra(EXTRA_GAME_ID, 0)
        if (gameId == 0L) {
            error("No game id")
        }
        GameViewModelFactory(application, gameId)
    }

    private lateinit var moveListAdapter: MoveListAdapter
    private var gameId: Long = 0
    private lateinit var localGameWithMoves: GameWithMoves
    private lateinit var localPosition: Position

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityGameBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        with(viewModel) {
            gameWithMoves.observe(this@GameActivity, {
                handleGameUpdate(it.game)
                handleMovesUpdate(it.moves)
                localGameWithMoves = it
            })
            selectedTile.observe(this@GameActivity) { coordinates: Coordinates? ->
                binding.content.board.selectTile(coordinates)
            }
            possibleMoves.observe(this@GameActivity) {
                binding.content.board.setPossibleMoves(it)
            }
            bottomPlayerSelectedCapturedPieceType.observe(this@GameActivity) { pieceType: PieceType? ->
                binding.content.ourCapturedPieces.selectPiece(pieceType)
            }
            topPlayerSelectedCapturedPieceType.observe(this@GameActivity) { pieceType: PieceType? ->
                binding.content.theirCapturedPieces.selectPiece(pieceType)
            }
            viewedPosition.observe(this@GameActivity, { position: Position ->
                binding.content.board.updatePosition(position)
                binding.content.ourCapturedPieces.updatePosition(position)
                binding.content.theirCapturedPieces.updatePosition(position)
                localPosition = position
            })
            playerToMove.observe(this@GameActivity, { playerToMove: RelativePlayer ->
                binding.content.topMoveIndication.visibility = View.INVISIBLE
                binding.content.bottomMoveIndication.visibility = View.INVISIBLE
                if (playerToMove == RelativePlayer.TOP) {
                    binding.content.topMoveIndication.visibility = View.VISIBLE
                } else if (playerToMove == RelativePlayer.BOTTOM) {
                    binding.content.bottomMoveIndication.visibility = View.VISIBLE
                }
            })
            showMakePromotingMoveButton.observe(this@GameActivity, { show: Boolean ->
                with(binding.content.moveWithPromotion) {
                    if (show) show() else hide()
                }
            })
            showMakeRegularMoveButton.observe(this@GameActivity, { show: Boolean ->
                with(binding.content.moveWithoutPromotion) {
                    if (show) show() else hide()
                }
            })
            selectedMoveEnd.observe(this@GameActivity, { end: Coordinates? -> binding.content.board.setMoveEnd(end) })
            toast.observe(this@GameActivity, {
                if (it == null) {
                    return@observe
                }
                val toastText = it.getContent()
                if (toastText != null) {
                    Toast.makeText(this@GameActivity, toastText, Toast.LENGTH_LONG).show()
                }
            })
            showProgressBar.observe(this@GameActivity, { binding.content.progressBar.visibility = if (it!!) View.VISIBLE else View.GONE })
        }


        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val recyclerView = binding.moveList
        moveListAdapter = MoveListAdapter(this@GameActivity)
        recyclerView.adapter = moveListAdapter

        with(binding) {
            startingPositionItem.setOnClickListener {
                viewModel.setViewedMove(0)
                drawerLayout.closeIfOpen()
            }
            currentPositionItem.setOnClickListener {
                viewModel.setViewedMove(-1)
                drawerLayout.closeIfOpen()
            }
            with(content) {
                layout.setOnClickListener {
                    viewModel.deselect()
                }
                board.setOnTileClickListener { _, coordinates: Coordinates ->
                    if (this@GameActivity::localGameWithMoves.isInitialized &&
                            this@GameActivity::localPosition.isInitialized) {
                        viewModel.boardTileClicked(localGameWithMoves.game, localPosition, coordinates)
                    }
                }
                moveWithoutPromotion.setOnClickListener {
                    if (this@GameActivity::localGameWithMoves.isInitialized &&
                            this@GameActivity::localPosition.isInitialized) {
                        viewModel.makeMove(localGameWithMoves.game, localPosition, false)
                    }
                }
                moveWithPromotion.setOnClickListener {
                    if (this@GameActivity::localGameWithMoves.isInitialized &&
                            this@GameActivity::localPosition.isInitialized) {
                        viewModel.makeMove(localGameWithMoves.game, localPosition, true)
                    }
                }
                progressBar.visibility = View.GONE
                ourCapturedPieces.setOnTileClickListener { _, pieceType: PieceType? ->
                    if (this@GameActivity::localGameWithMoves.isInitialized &&
                            this@GameActivity::localPosition.isInitialized) {
                        viewModel.playerCapturedPieceSelected(localGameWithMoves.game, localPosition, RelativePlayer.BOTTOM, pieceType)
                    }
                }
                theirCapturedPieces.setOnTileClickListener { _, pieceType: PieceType? ->
                    if (this@GameActivity::localGameWithMoves.isInitialized &&
                            this@GameActivity::localPosition.isInitialized) {
                        viewModel.playerCapturedPieceSelected(localGameWithMoves.game, localPosition, RelativePlayer.TOP, pieceType)
                    }
                }
                topMoveIndication.visibility = View.INVISIBLE
                bottomMoveIndication.visibility = View.INVISIBLE
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        super.onBackPressed()
        return true
    }

    override fun onBackPressed() {
        when {
            binding.drawerLayout.isDrawerOpen(GravityCompat.END) -> {
                binding.drawerLayout.closeDrawer(GravityCompat.END)
            }
            viewModel.selectedEnd() -> {
                viewModel.deselectEnd()
            }
            viewModel.selectedStart() -> {
                viewModel.deselect()
            }
            else -> {
                super.onBackPressed()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.game, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_game_info -> newInstance(gameId).show(supportFragmentManager, SHOW_GAME_INFO_DIALOG_TAG)
            R.id.action_show_chat -> {
                val intent = Intent(this@GameActivity, ChatActivity::class.java)
                intent.putExtra(ChatActivity.EXTRA_GAME_ID, gameId)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleGameUpdate(game: Game) {
        binding.gameName.text = game.name
        supportActionBar?.title = game.name
        binding.content.board.setPlayerBelow(game.ourPlayer)
        binding.content.ourCapturedPieces.setPlayerBelow(game.ourPlayer)
        binding.content.theirCapturedPieces.setPlayerBelow(game.ourPlayer)
    }

    private fun handleMovesUpdate(moves: List<GameMove>) {
        moveListAdapter.submitList(moves)
    }

    override fun onMoveListInteraction(move: GameMove) {
        viewModel.setViewedMove(move.numberFromStart)
        binding.drawerLayout.closeIfOpen()
    }

    companion object {
        private const val SHOW_GAME_INFO_DIALOG_TAG = "show_game_info"
        const val EXTRA_GAME_ID = "extra_game_id"
    }
}

fun DrawerLayout.closeIfOpen() {
    if (isDrawerOpen(GravityCompat.END)) {
        closeDrawer(GravityCompat.END)
    }
}