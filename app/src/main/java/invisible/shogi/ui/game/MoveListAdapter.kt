/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.game

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import invisible.shogi.R
import invisible.shogi.model.move.GameMove
import org.joda.time.format.DateTimeFormat

class MoveListAdapter(private val listener: ListInteractionListener?) : ListAdapter<GameMove, MoveListAdapter.ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_game_drawer_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class ViewHolder internal constructor(val item: View) : RecyclerView.ViewHolder(item) {
        private val moveString: TextView = item.findViewById(R.id.move_text)
        private val madeOn: TextView = item.findViewById(R.id.made_on)
        lateinit var move: GameMove

        fun bindTo(move: GameMove) {
            this.move = move
            moveString.text = String.format("%1\$d. %2\$s", move.numberFromStart, move.toNotationString())
            val instantMade = move.instantMade
            if (instantMade != null) {
                madeOn.text = instantMade.toString(DateTimeFormat.forPattern("yyyy-MM-dd\nHH:mm:ss"))
            }
            item.setOnClickListener { listener?.onMoveListInteraction(this.move) }
        }
    }

    interface ListInteractionListener {
        fun onMoveListInteraction(move: GameMove)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GameMove>() {
            override fun areItemsTheSame(oldItem: GameMove, newItem: GameMove) =
                    oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: GameMove, newItem: GameMove) =
                    areItemsTheSame(oldItem, newItem)
        }
    }
}