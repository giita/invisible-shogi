/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui

import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import invisible.shogi.DataRepository
import invisible.shogi.I2PUtil.generatePrivateKey
import invisible.shogi.InvisibleShogiApplication.Companion.appScope
import invisible.shogi.R
import invisible.shogi.databinding.ActivityMainBinding
import invisible.shogi.db.entity.GameInfo
import invisible.shogi.model.GameMode
import invisible.shogi.model.contact.Contact
import invisible.shogi.network.GameStartSenderForContact
import invisible.shogi.network.session.SessionManager.Companion.sessionManager
import invisible.shogi.ui.chat.ChatActivity
import invisible.shogi.ui.contactlist.ContactListFragment
import invisible.shogi.ui.dialog.EditContactDialogFragment
import invisible.shogi.ui.dialog.EditGameDialogFragment
import invisible.shogi.ui.dialog.GameInfoDialogFragment
import invisible.shogi.ui.game.GameActivity
import invisible.shogi.ui.gamelist.GameListFragment
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.i2p.client.streaming.I2PSocketManager
import net.i2p.data.Destination


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, ContactListFragment.OnListFragmentInteractionListener, GameListFragment.OnListFragmentInteractionListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var dataRepository: DataRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSupportActionBar(binding.toolbar)

        if (!getSharedPreferences("key", MODE_PRIVATE).contains("keyId")) {
            appScope.launch(IO) {
                val sharedPrivateKey = generatePrivateKey()
                val privateKeyId = DataRepository(this@MainActivity).savePrivateKey(sharedPrivateKey)
                getSharedPreferences("key", MODE_PRIVATE).edit().putLong("keyId", privateKeyId).apply()
            }
        }

        binding.navView.setNavigationItemSelectedListener(this)

        appBarConfiguration = AppBarConfiguration.Builder(R.id.contactListFragment,
                R.id.gameListFragment)
                .setOpenableLayout(binding.drawerLayout).build()
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)
        binding.navView.setCheckedItem(R.id.nav_games) // default destination
        dataRepository = DataRepository(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        return (NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp())
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_contacts -> navController.navigate(R.id.action_global_contactListFragment)
            R.id.nav_games -> navController.navigate(R.id.action_global_gameListFragment)
            R.id.nav_network_status -> navController.navigate(R.id.networkStatusFragment)
            R.id.nav_settings -> navController.navigate(R.id.settingsFragment)
            R.id.nav_about -> navController.navigate(R.id.aboutFragment)
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onContactListFragmentInteraction(contact: Contact) {
        val contactChatIntent = Intent(this, ChatActivity::class.java)
        contactChatIntent.putExtra(ChatActivity.EXTRA_CONTACT_ID, contact.id)
        startActivity(contactChatIntent)
    }

    override fun onGameListClick(game: GameInfo) {
        val openGameActivity = Intent(this, GameActivity::class.java)
        openGameActivity.putExtra(GameActivity.EXTRA_GAME_ID, game.id)
        startActivity(openGameActivity)
    }

    override fun onGameListCreateContextMenu(menu: ContextMenu, gameInfo: GameInfo) {
        val listener = GameMenuItemClickListener(gameInfo.id)
        val edit = menu.add(Menu.NONE, ITEM_EDIT, 1, "Edit")
        edit.setOnMenuItemClickListener(listener)
        val delete = menu.add(Menu.NONE, ITEM_DELETE, 2, "Delete")
        delete.setOnMenuItemClickListener(listener)
        if (!gameInfo.isArchived) {
            val archive = menu.add(Menu.NONE, ITEM_ARCHIVE, 3, "Archive")
            archive.setOnMenuItemClickListener(listener)
        }
        val showInfo = menu.add(Menu.NONE, ITEM_SHOW_INFO, 4, "Show info")
        showInfo.setOnMenuItemClickListener(listener)
    }

    override fun onCreateNewGame(mode: GameMode) {
        when (mode) {
            GameMode.LOCAL_PERSON,
            GameMode.LOCAL_ENGINE -> appScope.launch {
                val id = dataRepository.newLocalGame(mode)
                val openGameActivity = Intent(this@MainActivity, GameActivity::class.java)
                openGameActivity.putExtra(GameActivity.EXTRA_GAME_ID, id)
                startActivity(openGameActivity)
            }
            GameMode.I2P_DIRECT -> navController.navigate(R.id.connectDirectlyActivity)
            GameMode.I2P_RANDOM_MATCH_SERVER -> navController.navigate(R.id.randomMatchActivity)
        }
    }

    private inner class GameMenuItemClickListener(private val gameId: Long) : MenuItem.OnMenuItemClickListener {
        override fun onMenuItemClick(item: MenuItem): Boolean {
            when (item.itemId) {
                ITEM_EDIT -> EditGameDialogFragment.newInstance(gameId).show(supportFragmentManager, EDIT_GAME_DIALOG_TAG)
                ITEM_DELETE -> appScope.launch { dataRepository.deleteGame(gameId) }
                ITEM_ARCHIVE -> appScope.launch { dataRepository.archiveGame(gameId) }
                ITEM_SHOW_INFO -> GameInfoDialogFragment.newInstance(gameId).show(supportFragmentManager, SHOW_GAME_INFO_DIALOG_TAG)
            }
            return true
        }
    }

    override fun onContactListCreateContextMenu(menu: ContextMenu, contact: Contact) {
        val edit = menu.add(Menu.NONE, ITEM_EDIT, 1, "Edit")
        val delete = menu.add(Menu.NONE, ITEM_DELETE, 2, "Delete")
        val startNewGame = menu.add(Menu.NONE, ITEM_START_NEW_GAME, 3, "Start new game")
        val listener = ContactMenuItemClickListener(contact.id)
        edit.setOnMenuItemClickListener(listener)
        delete.setOnMenuItemClickListener(listener)
        startNewGame.setOnMenuItemClickListener(listener)
    }

    private inner class ContactMenuItemClickListener(private val contactId: Long) : MenuItem.OnMenuItemClickListener {
        override fun onMenuItemClick(item: MenuItem): Boolean {
            when (item.itemId) {
                ITEM_EDIT -> EditContactDialogFragment.newInstance(contactId).show(supportFragmentManager, EDIT_CONTACT_DIALOG_TAG)
                ITEM_DELETE -> appScope.launch { dataRepository.deleteContact(contactId) }
                ITEM_START_NEW_GAME -> lifecycleScope.launch(IO) {
                    val manager: I2PSocketManager = sessionManager.getManagerForKeyId(dataRepository.primaryKeyId)!!
                    if (manager == null) {
                        withContext(Main) {
                            Toast.makeText(this@MainActivity,
                                    "Session not running", Toast.LENGTH_LONG).show()
                        }
                        return@launch
                    }
                    val contact: Contact? = dataRepository.getContactById(contactId)
                    try {
                        val gameId = GameStartSenderForContact().sendGameRequestTo(this@MainActivity, manager.connect(Destination(contact!!.destination)))
                        startActivity(Intent(this@MainActivity, GameActivity::class.java)
                                .putExtra(GameActivity.EXTRA_GAME_ID, gameId))
                    } catch (e: Exception) {
                        withContext(Main) {
                            Toast.makeText(this@MainActivity,
                                    "Contact offline or declining", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
            return true
        }
    }

    companion object {
        private const val EDIT_CONTACT_DIALOG_TAG = "edit_contact"
        private const val EDIT_GAME_DIALOG_TAG = "edit_game"
        private const val SHOW_GAME_INFO_DIALOG_TAG = "show_game_info"
        private const val ITEM_EDIT = 1
        private const val ITEM_DELETE = 2
        private const val ITEM_START_NEW_GAME = 3
        private const val ITEM_SHOW_INFO = 4
        private const val ITEM_ARCHIVE = 5
    }
}