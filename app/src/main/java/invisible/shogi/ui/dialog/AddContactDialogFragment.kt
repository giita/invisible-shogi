/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.dialog

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import invisible.shogi.DataRepository
import invisible.shogi.InvisibleShogiApplication.Companion.appScope
import invisible.shogi.R
import invisible.shogi.model.contact.contact
import invisible.shogi.ui.QrCodeScanningActivity
import kotlinx.coroutines.launch

class AddContactDialogFragment : DialogFragment() {
    companion object {
        const val QR_CODE_RESULT_REQUEST = 0
    }

    private lateinit var innerView: View

    @SuppressLint("InflateParams") // android docs say that it's OK to pass null here
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        val builder = AlertDialog.Builder(requireActivity())
        innerView = inflater.inflate(R.layout.dialog_add_contact, null)
        val qrScanButton = innerView.findViewById<ImageButton>(R.id.scan_qr_button)
        qrScanButton.setOnClickListener { scanQR() }
        builder.setView(innerView)
                .setTitle(R.string.add_contact)
                .setPositiveButton(R.string.add) { _, _ ->
                    val nicknameView = innerView.findViewById<TextView>(R.id.input_nickname)
                    val destinationView = innerView.findViewById<TextView>(R.id.input_destination)
                    val newContact = contact(nicknameView.text.toString(),
                            destinationView.text.toString())
                    appScope.launch {
                        DataRepository(requireActivity().application).addContact(newContact)
                    }
                }
                .setNegativeButton(R.string.cancel) { _, _ -> }
        return builder.create()
    }

    private fun scanQR() {
        val intent = Intent(requireContext(), QrCodeScanningActivity::class.java)
        startActivityForResult(intent, QR_CODE_RESULT_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == QR_CODE_RESULT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                (innerView.findViewById<View>(R.id.input_destination) as TextView).text = data!!.getStringExtra(QrCodeScanningActivity.EXTRA_SCANNING_RESULT)
            }
        }
    }
}