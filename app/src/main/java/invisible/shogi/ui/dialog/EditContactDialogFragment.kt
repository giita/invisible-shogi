/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import invisible.shogi.DataRepository
import invisible.shogi.R
import invisible.shogi.db.entity.ContactEntity
import kotlinx.coroutines.launch

class EditContactDialogFragment private constructor() : DialogFragment() {
    private lateinit var innerView: View
    private var contact: ContactEntity? = null
    private lateinit var nicknameView: TextView

    @SuppressLint("InflateParams") // android docs say that it's OK to pass null here
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        val builder = AlertDialog.Builder(requireActivity())
        val args = arguments
        require(!(args == null || !args.containsKey(ARG_CONTACT_ID))) { "No arguments" }
        innerView = inflater.inflate(R.layout.dialog_edit_contact, null)
        nicknameView = innerView.findViewById(R.id.input_nickname)
        lifecycleScope.launch {
            val contact = DataRepository(requireActivity().application).getContactById(args.getLong(ARG_CONTACT_ID))
            this@EditContactDialogFragment.contact = contact
            nicknameView.text = contact.nickname
        }
        builder.setView(innerView)
                .setTitle(R.string.edit_contact)
                .setPositiveButton(R.string.save) { _, _ ->
                    contact!!.nickname = nicknameView.text.toString()
                    lifecycleScope.launch {
                        DataRepository(requireActivity().application).updateContact(contact!!)
                    }
                }
                .setNegativeButton(R.string.cancel) { _, _ -> }
        return builder.create()
    }

    companion object {
        private const val ARG_CONTACT_ID = "contact_id"
        @JvmStatic
        fun newInstance(contactId: Long): EditContactDialogFragment {
            val dialogFragment = EditContactDialogFragment()
            val args = Bundle()
            args.putLong(ARG_CONTACT_ID, contactId)
            dialogFragment.arguments = args
            return dialogFragment
        }
    }
}