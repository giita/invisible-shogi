/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import invisible.shogi.DataRepository
import invisible.shogi.R
import invisible.shogi.db.entity.GameEntity
import kotlinx.coroutines.launch

class EditGameDialogFragment private constructor() : DialogFragment() {
    private lateinit var innerView: View
    private var game: GameEntity? = null
    private lateinit var gameNameView: TextView

    @SuppressLint("InflateParams") // android docs say that it's OK to pass null here
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        val builder = AlertDialog.Builder(requireActivity())
        val args = arguments
        require(!(args == null || !args.containsKey(ARG_GAME_ID))) { "No arguments" }
        lifecycleScope.launch {
            val game = DataRepository(requireActivity().application).getGameById(args.getLong(ARG_GAME_ID))
            this@EditGameDialogFragment.game = game
            gameNameView.text = game.name
        }
        innerView = inflater.inflate(R.layout.dialog_edit_game, null)
        gameNameView = innerView.findViewById(R.id.input_game_name)
        builder.setView(innerView)
                .setTitle(R.string.edit_game)
                .setPositiveButton(R.string.save) { _, _ ->
                    game!!.name = gameNameView.text.toString()
                    lifecycleScope.launch {
                        DataRepository(requireActivity().application).updateGame(game!!)
                    }
                }
                .setNegativeButton(R.string.cancel) { _, _ -> }
        return builder.create()
    }

    companion object {
        private const val ARG_GAME_ID = "game_id"
        @JvmStatic
        fun newInstance(gameId: Long): EditGameDialogFragment {
            val dialogFragment = EditGameDialogFragment()
            val args = Bundle()
            args.putLong(ARG_GAME_ID, gameId)
            dialogFragment.arguments = args
            return dialogFragment
        }
    }
}