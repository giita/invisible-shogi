/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.google.zxing.WriterException
import invisible.shogi.R
import invisible.shogi.ui.QrUtil

class ShowQRDialogFragment private constructor() : DialogFragment() {
    @SuppressLint("InflateParams") // android docs say that it's OK to pass null here
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        val builder = AlertDialog.Builder(requireActivity())
        val innerView = inflater.inflate(R.layout.dialog_show_qr, null)
        val args = arguments
        require(!(args == null || !args.containsKey(ARG_CONTENT) || !args.containsKey(ARG_MESSAGE))) { "No arguments" }
        val content = args.getString(ARG_CONTENT)
                ?: throw IllegalArgumentException("Content can not be null")
        val imageView = innerView.findViewById<ImageView>(R.id.qr)
        try {
            imageView.setImageDrawable(QrUtil.drawableWithoutSmoothing(resources, QrUtil.toQrBitmap(content)))
        } catch (e: WriterException) {
            e.printStackTrace()
        }
        imageView.setOnClickListener {
            val clipboard = requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
            clipboard?.setPrimaryClip(ClipData.newPlainText("qr content", content))
            Toast.makeText(requireActivity(), "Copied the content to clipboard", Toast.LENGTH_SHORT).show()
        }
        builder.setView(innerView)
                .setMessage(args.getString(ARG_MESSAGE))
                .setPositiveButton("Close") { _, _ -> }
        return builder.create()
    }

    companion object {
        private const val ARG_CONTENT = "content"
        private const val ARG_MESSAGE = "message"
        fun newInstance(content: String?, message: String?): ShowQRDialogFragment {
            val dialogFragment = ShowQRDialogFragment()
            val args = Bundle()
            args.putString(ARG_CONTENT, content)
            args.putString(ARG_MESSAGE, message)
            dialogFragment.arguments = args
            return dialogFragment
        }
    }
}