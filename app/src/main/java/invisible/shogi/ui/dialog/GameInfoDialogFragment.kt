/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import invisible.shogi.DataRepository
import invisible.shogi.R
import kotlinx.coroutines.launch
import org.joda.time.format.DateTimeFormat

class GameInfoDialogFragment private constructor() : DialogFragment() {
    private lateinit var innerView: View
    private lateinit var gameInfoView: TextView

    @SuppressLint("InflateParams") // android docs say that it's OK to pass null here
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        val builder = AlertDialog.Builder(requireActivity())
        val args = arguments
        require(!(args == null || !args.containsKey(ARG_GAME_ID))) { "No arguments" }
        lifecycleScope.launch {
            val game = DataRepository(requireActivity().application).getGameById(args.getLong(ARG_GAME_ID))
            dialog!!.setTitle("Game " + game.name + " info")
            val info = StringBuilder()
            info.append("Started at " + game.startedAt!!.toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")))
            info.append('\n').append("Game mode: " + game.mode.toString())
            gameInfoView.text = info.toString()
        }
        innerView = inflater.inflate(R.layout.dialog_game_info, null)
        gameInfoView = innerView.findViewById(R.id.gameInfoView)
        builder.setView(innerView)
                .setTitle(R.string.game_info)
                .setPositiveButton("OK") { _, _ -> }
        return builder.create()
    }

    companion object {
        private const val ARG_GAME_ID = "game_id"
        @JvmStatic
        fun newInstance(gameId: Long): GameInfoDialogFragment {
            val dialogFragment = GameInfoDialogFragment()
            val args = Bundle()
            args.putLong(ARG_GAME_ID, gameId)
            dialogFragment.arguments = args
            return dialogFragment
        }
    }
}