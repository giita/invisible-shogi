/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import invisible.shogi.R
import invisible.shogi.databinding.FragmentNetworkStatusBinding
import invisible.shogi.service.NetworkService

class NetworkStatusFragment : Fragment() {
    private var bound = false
    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            binding.statusText.setText(R.string.service_found)
            bound = true
            val localBinder = binder as NetworkService.LocalBinder
            localBinder.service.sessionManager.info.observe(this@NetworkStatusFragment)
            { info: String? ->
                binding.serviceInformation.text = info
            }
            update()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            binding.statusText.setText(R.string.service_disconnected)
            bound = false
            update()
        }
    }
    private lateinit var binding: FragmentNetworkStatusBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentNetworkStatusBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        update()
    }

    private fun update() {
        setButtonStart(!bound)
    }

    private fun setButtonStart(start: Boolean) {
        if (start) {
            binding.button.setText(R.string.start_service)
            binding.button.setOnClickListener {
                requireActivity().startService(Intent(requireActivity(), NetworkService::class.java))
                requireActivity().bindService(Intent(requireActivity(), NetworkService::class.java), serviceConnection, 0)
            }
        } else {
            binding.button.setText(R.string.stop_service)
            binding.button.setOnClickListener {
                requireActivity().stopService(Intent(requireActivity(), NetworkService::class.java))
            }
        }
    }

    override fun onStart() {
        super.onStart()
        requireActivity().bindService(Intent(requireActivity(), NetworkService::class.java), serviceConnection, 0)
    }

    override fun onStop() {
        super.onStop()
        requireActivity().unbindService(serviceConnection)
    }
}