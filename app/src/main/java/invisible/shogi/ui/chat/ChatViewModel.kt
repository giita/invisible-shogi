/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.chat

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import invisible.shogi.DataRepository
import invisible.shogi.DataRepository.NetworkedActionResult
import invisible.shogi.InvisibleShogiApplication.Companion.appScope
import invisible.shogi.db.entity.MessageEntity
import invisible.shogi.ui.livedatautil.Event
import kotlinx.coroutines.launch
import org.joda.time.Instant

class ChatViewModel(application: Application, private var id: Long, private var contact: Boolean) : ViewModel() {
    var messages: LiveData<List<MessageEntity>>
        private set

    private val _toast = MutableLiveData<Event<String>?>(null)
    val toast: LiveData<Event<String>?>
        get() = _toast

    private val _waiting = MutableLiveData(false)
    val waiting: LiveData<Boolean>
        get() = _waiting

    private val _title = MutableLiveData("Chat")
    val title: LiveData<String>
        get() = _title

    private val repository: DataRepository = DataRepository(application)

    init {
        if (contact) {
            messages = repository.liveMessagesByContactId(id)
            viewModelScope.launch {
                val contactEntity = repository.getContactById(id)
                _title.postValue("Chat with " + contactEntity.nickname)
            }
        } else {
            messages = repository.liveMessagesByGameId(id)
            viewModelScope.launch {
                val game = repository.getGameById(id)
                _title.postValue("Chat for " + game.name)
            }
        }
    }

    fun sendMessage(messageText: String?) {
        _waiting.value = true
        appScope.launch label@ {
            val message = MessageEntity()
            if (contact) {
                message.contactId = id
            } else {
                message.gameId = id
            }
            message.content = messageText
            message.instantMade = Instant.now()
            message.byUs = true
            when (repository.sendMessage(message)) {
                NetworkedActionResult.SUCCESS -> return@label
                else -> _toast.postValue(Event("There was an error sending the message"))
            }
            _waiting.postValue(false)
        }
    }
}