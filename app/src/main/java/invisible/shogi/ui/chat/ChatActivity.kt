/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.chat

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.stfalcon.chatkit.commons.models.IMessage
import com.stfalcon.chatkit.commons.models.IUser
import com.stfalcon.chatkit.messages.MessagesListAdapter
import invisible.shogi.databinding.ActivityChatBinding
import invisible.shogi.db.entity.MessageEntity
import invisible.shogi.model.message.Message
import invisible.shogi.ui.livedatautil.Event
import java.util.*

class ChatActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChatBinding
    private val viewModel by viewModels<ChatViewModel> {
        val intent = intent ?: error("No intent")

        val contact = intent.hasExtra(EXTRA_CONTACT_ID)
        val game = intent.hasExtra(EXTRA_GAME_ID)

        if (contact && game || !(contact || game)) {
            error("Either contact or game should be provided")
        }

        val id = if (contact) intent.getLongExtra(EXTRA_CONTACT_ID, 0) else intent.getLongExtra(EXTRA_GAME_ID, 0)

        ChatViewModelFactory(application, id, contact)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityChatBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val adapter = MessagesListAdapter<ChatMessageImpl>("0", null)
        binding.messagesList.setAdapter(adapter)
        
        with(viewModel) {
            messages.observe(this@ChatActivity) { messageEntities: List<MessageEntity> ->
                val start = adapter.messagesCount
                for (i in start until messageEntities.size) {
                    adapter.addToStart(ChatMessageImpl(messageEntities[i]), true)
                }
            }
            title.observe(this@ChatActivity) { title: String? ->
                supportActionBar?.title = title
            }
            toast.observe(this@ChatActivity) { toastEvent: Event<String>? ->
                if (toastEvent == null) {
                    return@observe
                }
                val toastText = toastEvent.getContent()
                if (toastText != null) {
                    Toast.makeText(this@ChatActivity, toastText, Toast.LENGTH_LONG).show()
                }
            }
            waiting.observe(this@ChatActivity) {
                // TODO
            }
        }
        
        binding.input.setInputListener { input: CharSequence ->
            viewModel.sendMessage(input.toString())
            true
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(STATE_INPUT_TEXT, binding.input.inputEditText.text.toString())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        binding.input.inputEditText.setText(savedInstanceState.getString(STATE_INPUT_TEXT))
        super.onRestoreInstanceState(savedInstanceState)
    }

    class ChatMessageImpl(private val message: Message) : IMessage {
        override fun getId(): String {
            return ""
        }

        override fun getText(): String {
            return message.content!!
        }

        override fun getUser(): IUser {
            return object : IUser {
                override fun getId(): String {
                    return if (message.byUs) "0" else "1"
                }

                override fun getName(): String {
                    return "Anon"
                }

                override fun getAvatar(): String? {
                    return null
                }
            }
        }

        override fun getCreatedAt(): Date {
            return message.instantMade!!.toDate()
        }
    }

    companion object {
        const val EXTRA_CONTACT_ID = "contact_id"
        const val EXTRA_GAME_ID = "game_id"
        private const val STATE_INPUT_TEXT = "state_input_text"
    }
}