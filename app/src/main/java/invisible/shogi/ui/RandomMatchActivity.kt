/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import invisible.shogi.R
import invisible.shogi.network.CentralizedRandomMatchReceiver
import invisible.shogi.network.session.SessionInfo
import invisible.shogi.network.session.SessionManager.Companion.sessionManager

class RandomMatchActivity : AppCompatActivity() {
    private var info: SessionInfo? = null
    private var findMatchButton: Button = findViewById(R.id.findMatch)
    private var status: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_random_match)
        if (!PreferenceManager.getDefaultSharedPreferences(applicationContext).contains(MATCHMAKING_SERVER_SETTING)) {
            val alertBuilder = AlertDialog.Builder(this)
            alertBuilder.setMessage("Matchmaking server not set up. Please fill it in in the Settings.")
            alertBuilder.setPositiveButton("OK", null)
            alertBuilder.setOnDismissListener { finish() }
            alertBuilder.create().show()
        }
        findMatchButton.setOnClickListener {
            findMatchButton.isEnabled = false
            sessionManager.newSession("Matchmaking session", CentralizedRandomMatchReceiver(this) {
                sessionManager.startSessions()
                finish()})
//            }) { info: SessionManager.SessionInfo ->
//                this@RandomMatchActivity.info = info
//                if (info.status == SessionManager.MANAGER_CREATED) {
//                    status!!.setText(R.string.connecting_ephemeral_session)
//                } else if (info.status == SessionManager.CONNECTION_FINISHED) {
//                    status!!.setText(R.string.talking_with_matchmaker)
//                    val matchMakerDestination = PreferenceManager.getDefaultSharedPreferences(applicationContext).getString(MATCHMAKING_SERVER_SETTING, "")
//                    applicationScope.launch(IO) {
//                        try {
//                            CentralizedRandomMatchRequester().handle(info.manager!!.connect(Destination(matchMakerDestination)))
//                        } catch (e: DataFormatException) {
//                            withContext(Main) { status!!.setText(R.string.wrong_destination_format) }
//                            return@launch
//                        } catch (e: Exception) {
//                            withContext(Main) { status!!.setText(R.string.error_talking_to_matchmaker) }
//                            return@launch
//                        }
//                        withContext(Main) { status!!.setText(R.string.waiting_for_response) }
//                    }
//                }

        }
        status = findViewById(R.id.status)
    }

    override fun onDestroy() {
        //sessionManager.removeEphemeralSession(info)
        super.onDestroy()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        private const val MATCHMAKING_SERVER_SETTING = "matchmaking_server_destination"
    }
}