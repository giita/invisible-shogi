/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.gamelist

import android.content.Context
import android.os.Bundle
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import invisible.shogi.R
import invisible.shogi.databinding.FragmentGameListBinding
import invisible.shogi.db.entity.GameInfo
import invisible.shogi.model.GameMode

class GameListFragment : Fragment() {
    private lateinit var pagerAdapter: FragmentPagerAdapter
    private lateinit var binding: FragmentGameListBinding
    private var _listener: OnListFragmentInteractionListener? = null
    private val listener
        get() = _listener!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentGameListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        _listener = if (context is OnListFragmentInteractionListener) {
            context
        } else {
            throw RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        _listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.optionLocal.setOnClickListener { listener.onCreateNewGame(GameMode.LOCAL_PERSON) }
        binding.optionWithEngine.setOnClickListener { listener.onCreateNewGame(GameMode.LOCAL_ENGINE) }
        binding.optionDirect.setOnClickListener { listener.onCreateNewGame(GameMode.I2P_DIRECT) }
        binding.optionRandomMatch.setOnClickListener { listener.onCreateNewGame(GameMode.I2P_RANDOM_MATCH_SERVER) }

        pagerAdapter = GameListPagerAdapter(childFragmentManager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT)
        binding.pager.adapter = pagerAdapter
        binding.pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> binding.fab.show()
                    else -> binding.fab.hide()
                }
            }
            override fun onPageScrollStateChanged(state: Int) {}
        })
        binding.tabLayout.setupWithViewPager(binding.pager)
    }

    inner class GameListPagerAdapter(fm: FragmentManager, behavior: Int) : FragmentPagerAdapter(fm, behavior) {
        override fun getItem(position: Int) =
                when (position) {
                    0 -> ListFragment(listener, ListType.ONGOING)
                    else -> ListFragment(listener, ListType.ARCHIVED)
                }

        override fun getCount() = 2

        override fun getPageTitle(position: Int) =
                when (position) {
                    0 -> getString(R.string.ongoing)
                    else -> getString(R.string.archived)
                }
    }

    class ListFragment(private val listener: OnListFragmentInteractionListener,
                       private val type: ListType) : Fragment() {
        private val viewModel: GameListViewModel by viewModels(ownerProducer = { requireParentFragment() })
        private lateinit var adapter: ListAdapter<GameInfo, *>

        override fun onCreateView(inflater: LayoutInflater,
                                  container: ViewGroup?, savedInstanceState: Bundle?): View {
            return inflater.inflate(when (type) {
                ListType.ONGOING -> R.layout.fragment_ongoing_games_list
                ListType.ARCHIVED -> R.layout.fragment_archived_games_list
            }, container, false)
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            val games = when (type) {
                ListType.ONGOING -> viewModel.ongoingGames
                ListType.ARCHIVED -> viewModel.archivedGames
            }
            games.observe(viewLifecycleOwner, { adapter.submitList(it) })
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            val list: RecyclerView = view.findViewById(when (type) {
                ListType.ONGOING -> R.id.list_ongoing
                ListType.ARCHIVED -> R.id.list_archived
            })
            val contextOngoing = list.context
            list.layoutManager = LinearLayoutManager(contextOngoing)
            adapter = when (type) {
                ListType.ONGOING -> OngoingGamesListAdapter(listener)
                ListType.ARCHIVED -> ArchivedGamesListAdapter(listener)
            }
            list.adapter = adapter
        }
    }

    enum class ListType {
        ONGOING,
        ARCHIVED
    }

    interface OnListFragmentInteractionListener {
        fun onGameListClick(game: GameInfo)
        fun onGameListCreateContextMenu(menu: ContextMenu, gameInfo: GameInfo)
        fun onCreateNewGame(mode: GameMode)
    }
}