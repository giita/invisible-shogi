/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.gamelist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import invisible.shogi.DataRepository
import invisible.shogi.db.entity.GameInfo

class GameListViewModel(application: Application) : AndroidViewModel(application) {
    val dataRepository = DataRepository(application)
    val ongoingGames: LiveData<List<GameInfo>> = dataRepository.liveOngoingGamesBriefs
    val archivedGames: LiveData<List<GameInfo>> = dataRepository.liveArchivedGamesBriefs
}