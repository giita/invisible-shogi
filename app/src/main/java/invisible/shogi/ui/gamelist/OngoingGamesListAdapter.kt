/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.gamelist

import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnCreateContextMenuListener
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import invisible.shogi.databinding.FragmentOngoingGamesListItemBinding
import invisible.shogi.db.entity.GameInfo
import org.joda.time.format.DateTimeFormat

class OngoingGamesListAdapter(private val listener: GameListFragment.OnListFragmentInteractionListener) : ListAdapter<GameInfo, OngoingGamesListAdapter.ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = FragmentOngoingGamesListItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class ViewHolder(private val binding: FragmentOngoingGamesListItemBinding) :
            RecyclerView.ViewHolder(binding.root), OnCreateContextMenuListener {
        lateinit var gameInfo: GameInfo

        fun bindTo(info: GameInfo) {
            gameInfo = info
            binding.gameName.text = info.name
            binding.startedAt.text = String.format("Started at\n%1\$s", info.startedAt.toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")))
            binding.moveNumber.text = "Move: ${info.currentMoveNumber}"
            binding.typeIcon.setImageDrawable(ResourcesCompat.getDrawable(binding.root.context.resources, info.mode.resourceId, null))
            binding.root.setOnClickListener { listener.onGameListClick(gameInfo) }
        }

        override fun onCreateContextMenu(menu: ContextMenu, v: View?, menuInfo: ContextMenuInfo?) {
            listener.onGameListCreateContextMenu(menu, gameInfo)
        }

        init {
            binding.root.setOnCreateContextMenuListener(this)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GameInfo>() {
            override fun areItemsTheSame(oldItem: GameInfo, newItem: GameInfo) =
                    oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: GameInfo, newItem: GameInfo) =
                    oldItem.name == newItem.name && oldItem.currentMoveNumber == newItem.currentMoveNumber
        }
    }
}