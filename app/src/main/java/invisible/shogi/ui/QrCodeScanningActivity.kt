/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import invisible.shogi.R
import me.dm7.barcodescanner.zxing.ZXingScannerView
import me.dm7.barcodescanner.zxing.ZXingScannerView.ResultHandler

class QrCodeScanningActivity : AppCompatActivity(), ResultHandler {
    private lateinit var scannerView: ZXingScannerView
    private var canStartCamera = false
    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        setContentView(R.layout.activity_qr_code_scanning)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val contentFrame = findViewById<ViewGroup>(R.id.content_frame)
        scannerView = ZXingScannerView(this).apply {
            setFormats(listOf(BarcodeFormat.QR_CODE))
            setLaserEnabled(false)
            setSquareViewFinder(true)
            val colorPrimary = TypedValue()
            theme.resolveAttribute(R.attr.colorPrimary, colorPrimary, true)
            setBorderColor(colorPrimary.data)
            setResultHandler(this@QrCodeScanningActivity)
        }
        contentFrame.addView(scannerView)

        checkCameraPermission()
    }

    private fun checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), ZXING_CAMERA_PERMISSION)
        } else {
            canStartCamera = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == ZXING_CAMERA_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                scannerView.startCamera()
                canStartCamera = true
            } else {
                Toast.makeText(this, getString(R.string.please_grant_camera_permission), Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    public override fun onResume() {
        super.onResume()
        if (canStartCamera) {
            scannerView.startCamera()
        }
    }

    public override fun onPause() {
        super.onPause()
        if (canStartCamera) {
            scannerView.stopCamera()
        }
    }

    override fun handleResult(rawResult: Result) {
        val data = Intent()
        data.putExtra(EXTRA_SCANNING_RESULT, rawResult.text)
        setResult(RESULT_OK, data)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val EXTRA_SCANNING_RESULT = "scanning_result"
        const val ZXING_CAMERA_PERMISSION = 0
    }
}