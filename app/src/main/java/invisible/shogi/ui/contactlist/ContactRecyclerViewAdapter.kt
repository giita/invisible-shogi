/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.contactlist

import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnCreateContextMenuListener
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import invisible.shogi.R
import invisible.shogi.model.contact.Contact

class ContactRecyclerViewAdapter(private val listener: ContactListFragment.OnListFragmentInteractionListener?) : ListAdapter<Contact, ContactRecyclerViewAdapter.ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_contact_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view), OnCreateContextMenuListener {
        private val contactName: TextView = view.findViewById(R.id.contact_name)
        lateinit var contact: Contact

        fun bindTo(contact: Contact) {
            this.contact = contact
            contactName.text = contact.nickname
            view.setOnClickListener { listener?.onContactListFragmentInteraction(this.contact) }
        }

        override fun onCreateContextMenu(menu: ContextMenu, v: View?, menuInfo: ContextMenuInfo?) {
            listener?.onContactListCreateContextMenu(menu, contact)
        }

        init {
            view.setOnCreateContextMenuListener(this)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Contact>() {
            override fun areItemsTheSame(oldItem: Contact, newItem: Contact) =
                    oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Contact, newItem: Contact) =
                    oldItem.nickname == newItem.nickname
        }
    }
}