/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.ui.contactlist

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import invisible.shogi.DataRepository
import invisible.shogi.I2PUtil.destinationFromKey
import invisible.shogi.InvisibleShogiApplication.Companion.appScope
import invisible.shogi.R
import invisible.shogi.model.contact.Contact
import invisible.shogi.ui.dialog.AddContactDialogFragment
import invisible.shogi.ui.dialog.ShowQRDialogFragment
import kotlinx.coroutines.launch
import net.i2p.data.DataFormatException

class ContactListFragment : Fragment() {
    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var adapter: ContactRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        val view = inflater.inflate(R.layout.fragment_contact_list, container, false)
        val rec = view.findViewById<View>(R.id.contact_list)
        if (rec is RecyclerView) {
            val context = rec.getContext()
            rec.layoutManager = LinearLayoutManager(context)
            adapter = ContactRecyclerViewAdapter(listener)
            rec.adapter = adapter
        }
        val fab: FloatingActionButton = view.findViewById(R.id.fab)
        fab.setOnClickListener { AddContactDialogFragment().show(requireActivity().supportFragmentManager, ADD_CONTACT_DIALOG_TAG) }
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.contact_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_show_my_pid -> appScope.launch {
                val dataRepository = DataRepository(requireActivity())
                val keyId = dataRepository.primaryKeyId
                val privateKey = dataRepository.getPrivateKey(keyId)
                try {
                    ShowQRDialogFragment.newInstance(destinationFromKey(privateKey).toBase64(), "Your persistent ID")
                            .show(requireActivity().supportFragmentManager, SHOW_MY_PID_DIALOG_TAG)
                } catch (e: DataFormatException) {
                    e.printStackTrace()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel = ViewModelProvider(this).get(ContactListViewModel::class.java)
        viewModel.contacts.observe(viewLifecycleOwner, { adapter.submitList(it) })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = if (context is OnListFragmentInteractionListener) {
            context
        } else {
            throw RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnListFragmentInteractionListener {
        fun onContactListFragmentInteraction(contact: Contact)
        fun onContactListCreateContextMenu(menu: ContextMenu, contact: Contact)
    }

    companion object {
        private const val SHOW_MY_PID_DIALOG_TAG = "show_my_pid"
        private const val ADD_CONTACT_DIALOG_TAG = "add_contact"
    }
}