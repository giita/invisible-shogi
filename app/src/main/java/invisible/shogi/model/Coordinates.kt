/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.model

import invisible.shogi.logic.Position

data class Coordinates(val row: Int, val column: Int) {
    // Not for use in notation
    fun toWesternNotation(): String {
        return "" + (Position.BOARD_SIZE - column) + ('a'.toInt() + row).toChar()
    }

    fun toJapaneseNotation(): String {
        return "" + (Position.BOARD_SIZE - column) + asJapaneseNumeral(row + 1)
    }

    fun inBounds(boardSide: Int): Boolean {
        return row in 0 until boardSide && column in 0 until boardSide
    }

    companion object {
        fun fromWesternNotation(s: String): Coordinates {
            return Coordinates(s[1] - 'a', Position.BOARD_SIZE - Integer.valueOf(s[0].toString()))
        }
    }
}

fun asJapaneseNumeral(i: Int): String {
    return when (i) {
        1 -> "一"
        2 -> "二"
        3 -> "三"
        4 -> "四"
        5 -> "五"
        6 -> "六"
        7 -> "七"
        8 -> "八"
        9 -> "九"
        else -> throw IllegalArgumentException()
    }
}