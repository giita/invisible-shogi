/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.model

enum class PieceType(override val code: Int) : IntEnum {
    KING(0),
    ROOK(1),
    PROMOTED_ROOK(2),
    BISHOP(3),
    PROMOTED_BISHOP(4),
    GOLD(5),
    SILVER(6),
    PROMOTED_SILVER(7),
    KNIGHT(8),
    PROMOTED_KNIGHT(9),
    LANCE(10),
    PROMOTED_LANCE(11),
    PAWN(12),
    PROMOTED_PAWN(13);

    val isPromoted: Boolean
        get() {
            return when (this) {
                PROMOTED_ROOK,
                PROMOTED_BISHOP,
                PROMOTED_SILVER,
                PROMOTED_KNIGHT,
                PROMOTED_LANCE,
                PROMOTED_PAWN -> true
                else -> false
            }
        }

    val canBePromoted: Boolean
        get() {
            return when (this) {
                ROOK,
                BISHOP,
                SILVER,
                KNIGHT,
                LANCE,
                PAWN -> true
                else -> false
            }
        }

    val promoted: PieceType
        get() {
            require(canBePromoted)
            return when (this) {
                ROOK -> PROMOTED_ROOK
                BISHOP -> PROMOTED_BISHOP
                SILVER -> PROMOTED_SILVER
                KNIGHT -> PROMOTED_KNIGHT
                LANCE -> PROMOTED_LANCE
                PAWN -> PROMOTED_PAWN
                else -> error("Unreachable")
            }
        }

    val unpromoted: PieceType
        get() {
            require(isPromoted)
            return when (this) {
                PROMOTED_ROOK -> ROOK
                PROMOTED_BISHOP -> BISHOP
                PROMOTED_SILVER -> SILVER
                PROMOTED_KNIGHT -> KNIGHT
                PROMOTED_LANCE -> LANCE
                PROMOTED_PAWN -> PAWN
                else -> error("Unreachable")
            }
        }
}

