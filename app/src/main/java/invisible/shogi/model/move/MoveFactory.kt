/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.model.move

import invisible.shogi.model.Coordinates
import invisible.shogi.model.Piece
import org.joda.time.Instant

/**
 * Methods for creating various local moves. Note that a separate class is used, not the MoveEntity
 * that is returned by Room.
 */
object MoveFactory {
    fun startDrop(piece: Piece): StartedMove {
        val result = GameMoveImpl()
        result.piece = piece
        result.pieceCoordinatesBefore = null
        return result
    }

    fun startMove(piece: Piece, coordinatesBefore: Coordinates): StartedMove {
        val result = GameMoveImpl()
        result.piece = piece
        result.pieceCoordinatesBefore = coordinatesBefore
        return result
    }

    fun finishMove(startedMove: StartedMove,
                   coordinatesAfter: Coordinates,
                   promoting: Boolean): Move {
        val result = GameMoveImpl(startedMove)
        result.pieceCoordinatesAfter = coordinatesAfter
        result.isPromoting = promoting
        return result
    }

    fun makeMoveIn(move: Move,
                   gameId: Long,
                   numberFromStart: Int,
                   instantMade: Instant?,
                   instantReceived: Instant?): GameMove {
        val result = GameMoveImpl(move)
        result.instantMade = instantMade
        result.instantReceived = instantReceived
        //result.id = 0; // determined by database
        result.gameId = gameId
        result.numberFromStart = numberFromStart
        return result
    }

    // TODO: make most things private val
    class GameMoveImpl : GameMove {
        override lateinit var piece: Piece
        override var pieceCoordinatesBefore: Coordinates? = null
        override lateinit var pieceCoordinatesAfter: Coordinates
        override var isPromoting = false
        override var instantMade: Instant? = null
        override var instantReceived: Instant? = null
        override var gameId: Long = 0
        override var numberFromStart = 0

        constructor()
        constructor(startedMove: StartedMove) {
            piece = startedMove.piece
            pieceCoordinatesBefore = startedMove.pieceCoordinatesBefore
        }

        constructor(move: Move) : this(move as StartedMove) {
            pieceCoordinatesAfter = move.pieceCoordinatesAfter
            isPromoting = move.isPromoting
        }

        override val id: Long
            get() {
                throw IllegalStateException("This move does not have an id")
            }
    }
}