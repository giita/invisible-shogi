/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.model.move

import invisible.shogi.model.Coordinates
import invisible.shogi.model.Piece
import invisible.shogi.model.PieceType
import invisible.shogi.model.Player

/** Move that has a piece and the starting position already, but not the target position yet.  */
interface StartedMove {
    val piece: Piece

    val pieceType: PieceType
        get() = piece.type

    val player: Player
        get() = piece.player

    val pieceCoordinatesBefore: Coordinates?

    fun requirePieceCoordinatesBefore(): Coordinates {
        return pieceCoordinatesBefore
                ?: throw IllegalStateException("Trying to requirePieceCoordinatesBefore on a drop")
    }

    val isDrop: Boolean
        get() = pieceCoordinatesBefore == null
}