/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.model

import invisible.shogi.R

enum class GameMode(override val code: Int) : IntEnum {
    LOCAL_PERSON(0),
    LOCAL_ENGINE(1),
    I2P_DIRECT(2),
    I2P_RANDOM_MATCH_SERVER(3);

    override fun toString() = when (this) {
        LOCAL_PERSON -> "Local"
        LOCAL_ENGINE -> "Local with engine"
        I2P_DIRECT -> "I2P direct"
        I2P_RANDOM_MATCH_SERVER -> "I2P random match"
    }

    val resourceId: Int
        get() = when (this) {
            LOCAL_PERSON -> R.drawable.ic_baseline_group
            LOCAL_ENGINE -> R.drawable.ic_baseline_memory
            I2P_DIRECT -> R.drawable.ic_compare_arrows_black
            I2P_RANDOM_MATCH_SERVER -> R.drawable.ic_random
        }
}