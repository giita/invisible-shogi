/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.model

import invisible.shogi.R

/** Class representing a piece type together with the player the piece belongs to  */
data class Piece(val player: Player, val type: PieceType) {
    val resourceId: Int
        get() {
            return when (type) {
                PieceType.KING ->
                    if (player == Player.BLACK)
                        R.drawable.piece_black_king
                    else
                        R.drawable.piece_white_king
                PieceType.ROOK -> R.drawable.piece_rook
                PieceType.PROMOTED_ROOK -> R.drawable.piece_promoted_rook
                PieceType.BISHOP -> R.drawable.piece_bishop
                PieceType.PROMOTED_BISHOP -> R.drawable.piece_promoted_bishop
                PieceType.GOLD -> R.drawable.piece_gold
                PieceType.SILVER -> R.drawable.piece_silver
                PieceType.PROMOTED_SILVER -> R.drawable.piece_promoted_silver
                PieceType.KNIGHT -> R.drawable.piece_knight
                PieceType.PROMOTED_KNIGHT -> R.drawable.piece_promoted_knight
                PieceType.LANCE -> R.drawable.piece_lance
                PieceType.PROMOTED_LANCE -> R.drawable.piece_promoted_lance
                PieceType.PAWN -> R.drawable.piece_pawn
                PieceType.PROMOTED_PAWN -> R.drawable.piece_promoted_pawn
            }
        }

    fun toJapaneseNotation(): String {
        return when (type) {
            PieceType.KING -> if (player == Player.BLACK) "王" else "玉"
            PieceType.ROOK -> "飛"
            PieceType.PROMOTED_ROOK -> "龍"
            PieceType.BISHOP -> "角"
            PieceType.PROMOTED_BISHOP -> "馬"
            PieceType.GOLD -> "金"
            PieceType.SILVER -> "銀"
            PieceType.PROMOTED_SILVER -> "成銀"
            PieceType.KNIGHT -> "桂"
            PieceType.PROMOTED_KNIGHT -> "成桂"
            PieceType.LANCE -> "香"
            PieceType.PROMOTED_LANCE -> "成香"
            PieceType.PAWN -> "歩"
            PieceType.PROMOTED_PAWN -> "と"
        }
    }
}