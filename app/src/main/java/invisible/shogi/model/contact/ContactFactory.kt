/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.model.contact

fun contact(nickname: String, id: String): Contact {
    return ContactImpl(nickname, id)
}

private data class ContactImpl(override val nickname: String, override val destination: String) : Contact {
    override val id: Long
        get() {
            throw IllegalStateException("This contact does not have an id yet")
        }
}