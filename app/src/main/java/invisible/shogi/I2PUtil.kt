/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi

import android.util.Log
import net.i2p.I2PException
import net.i2p.client.I2PClientFactory
import net.i2p.crypto.SigType
import net.i2p.data.DataFormatException
import net.i2p.data.Destination
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream



object I2PUtil {
    private const val TAG = "I2PUtil"

    fun generatePrivateKey(): ByteArray {
        val arrayStream = ByteArrayOutputStream()
        try {
            I2PClientFactory.createClient().createDestination(arrayStream, SigType.RedDSA_SHA512_Ed25519)
        } catch (e: I2PException) {
            Log.e(TAG, "could not generate key: " + e.message)
            throw RuntimeException(e)
        }
        return arrayStream.toByteArray()
    }

    @Throws(DataFormatException::class)
    fun destinationFromKey(key: ByteArray?): Destination {
        val inputStream = ByteArrayInputStream(key)
        val destination = Destination.create(inputStream)
        if (BuildConfig.DEBUG && destination == null) {
            error("Assertion failed")
        }
        return destination
    }
}
