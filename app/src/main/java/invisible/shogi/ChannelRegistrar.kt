/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.content.ContextCompat

/**
 * Since Android O notifications should be posted to separate channels. This class registers
 * all the needed channels. Since channels can be created multiple times a good time to use this
 * is on every application start.
 */
class ChannelRegistrar(private val context: Context) {
    private var notificationManager: NotificationManager? = null
    fun registerChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager = ContextCompat.getSystemService(context, NotificationManager::class.java)
        }
        registerStatusNotificationChannel()
        registerNewMessageNotificationChannel()
        registerNewGameNotificationChannel()
    }

    private fun registerStatusNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = context.getString(R.string.channel_name)
            val description = context.getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(STATUS_NOTIFICATION_CHANNEL_ID, name, importance)
            channel.description = description
            notificationManager!!.createNotificationChannel(channel)
        }
    }

    private fun registerNewMessageNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(NEW_MESSAGE_CHANNEL_ID, "New messages", NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = "Channel for new messages in games and with contacts"
            notificationManager!!.createNotificationChannel(channel)
        }
    }

    private fun registerNewGameNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(NEW_GAME_CHANNEL_ID, "New games", NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = "Channel for notifications about new games"
            notificationManager!!.createNotificationChannel(channel)
        }
    }

    companion object {
        var STATUS_NOTIFICATION_CHANNEL_ID = "network_status_channel"
        var NEW_MESSAGE_CHANNEL_ID = "new_message_channel"
        var NEW_GAME_CHANNEL_ID = "new_game_channel"
    }
}