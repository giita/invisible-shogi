/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.service

import android.app.Notification
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.LifecycleService
import invisible.shogi.ChannelRegistrar
import invisible.shogi.R
import invisible.shogi.network.session.SessionManager
import invisible.shogi.ui.MainActivity

/** Service for keeping SessionManager alive and showing a notification  */
class NetworkService : LifecycleService() {
    private val binder: IBinder = LocalBinder()

    inner class LocalBinder : Binder() {
        val service
            get() = this@NetworkService
    }

    override fun onBind(intent: Intent): IBinder? {
        super.onBind(intent)
        return binder
    }

    class NetworkServiceBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action != null && action == ACTION_STOP_SERVICE) {
                context.stopService(Intent(context, NetworkService::class.java))
            }
        }
    }

    private fun prepareNotification(): NotificationCompat.Builder {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val shutdownIntent = Intent(ACTION_STOP_SERVICE)
        shutdownIntent.setClass(this, NetworkServiceBroadcastReceiver::class.java)
        val shutdownPendingIntent = PendingIntent.getBroadcast(this, 0, shutdownIntent, 0)
        return NotificationCompat.Builder(this, ChannelRegistrar.STATUS_NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.piece_black_king)
                .setContentTitle(getString(R.string.service_is_running))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .addAction(0, getString(R.string.stop_service), shutdownPendingIntent)
                .setOngoing(true)
    }

    override fun onCreate() {
        super.onCreate()
        startForeground(NOTIFICATION_ID, updateNotification("Starting the service"))
        sessionManager.info.observe(this, { info: String -> updateNotification(info) })
        sessionManager.startSessions()
    }

    override fun onDestroy() {
        sessionManager.stopSessions()
        super.onDestroy()
    }

    private fun updateNotification(info: String): Notification {
        val notificationManager = NotificationManagerCompat.from(this)
        val builder = prepareNotification()
        builder.setContentText(getString(R.string.more_info_inside))
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(info))
        val notification = builder.build()
        notificationManager.notify(NOTIFICATION_ID, notification)
        return notification
    }

    val sessionManager = SessionManager.sessionManager

    companion object {
        private const val NOTIFICATION_ID = 1
        private const val ACTION_STOP_SERVICE = "invisible.shogi.action.STOP_NETWORK_SERVICE"
    }
}