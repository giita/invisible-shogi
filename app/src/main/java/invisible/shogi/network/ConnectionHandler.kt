/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import net.i2p.I2PException
import net.i2p.client.streaming.I2PSocket
import java.io.IOException

/**
 * Does something over a socket. Mainly used for the server part, where we pass the handler
 * when creating server listener.
 */
interface ConnectionHandler {
    @Throws(IOException::class, I2PException::class)
    suspend fun handle(socket: I2PSocket)
}