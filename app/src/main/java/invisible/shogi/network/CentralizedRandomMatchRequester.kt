/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import invisible.shogi.network.Common.REPLY_ACK
import invisible.shogi.network.Common.RemoteDeclinedException
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import net.i2p.client.streaming.I2PSocket
import org.joda.time.Duration
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException

class CentralizedRandomMatchRequester : ConnectionHandler {
    @Throws(IOException::class)
    override suspend fun handle(socket: I2PSocket) = withContext(IO) {
        val dataInputStream = DataInputStream(socket.inputStream)
        val dataOutputStream = DataOutputStream(socket.outputStream)
        // TODO ask the user for this value and consider it in UI
        val duration = Duration.standardMinutes(5)
        dataOutputStream.writeLong(duration.standardSeconds) // how much we are going to wait for
        dataOutputStream.flush()
        if (dataInputStream.readInt() != REPLY_ACK) {
            throw RemoteDeclinedException()
        }
    }
}