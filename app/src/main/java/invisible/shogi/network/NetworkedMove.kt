/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import invisible.shogi.model.Coordinates
import invisible.shogi.model.Piece
import invisible.shogi.model.move.Move

/** An implementation of Move to be sent and received over network  */
internal class NetworkedMove(move: Move) : Move {
    override val piece: Piece = move.piece
    override val pieceCoordinatesBefore: Coordinates? = move.pieceCoordinatesBefore
    override val pieceCoordinatesAfter: Coordinates = move.pieceCoordinatesAfter
    override val isPromoting: Boolean = move.isPromoting
}