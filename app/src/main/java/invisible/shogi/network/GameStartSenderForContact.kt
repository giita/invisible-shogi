/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import android.content.Context
import invisible.shogi.DataRepository
import invisible.shogi.db.entity.GameEntity
import invisible.shogi.model.GameMode
import invisible.shogi.model.Player
import invisible.shogi.network.Common.RemoteDeclinedException
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import net.i2p.client.streaming.I2PSocket
import org.joda.time.Instant
import java.io.*
import java.util.*

class GameStartSenderForContact {
    @Throws(IOException::class)
    suspend fun sendGameRequestTo(context: Context?, socket: I2PSocket): Long = withContext(IO) {
        val reader = InputStreamReader(socket.inputStream)
        val dataInput = DataInputStream(socket.inputStream)
        val writer = OutputStreamWriter(socket.outputStream)
        val dataOutput = DataOutputStream(socket.outputStream)
        dataOutput.writeInt(ConnectionReceiver.MESSAGE_TYPE_START_GAME_CONTACT)
        dataOutput.flush()
        val newMyUuid = UUID.randomUUID()
        Common.writeUuid(newMyUuid, dataOutput)
        val newRemoteUuid = Common.readUuid(dataInput)
        dataOutput.flush()
        val reply = dataInput.readInt()
        if (reply != Common.REPLY_ACK) {
            throw RemoteDeclinedException()
        }
        val dataRepository = DataRepository(context!!)

        // TODO who's the starter? not me!
        // TODO what's the starting position?
        val game = GameEntity().apply {
            ourPlayer = Player.WHITE
            mode = GameMode.I2P_DIRECT
            startedAt = Instant.now()
            name = dataRepository.getNextGameName()
            privateKeyId = dataRepository.primaryKeyId
            remoteDestination = socket.peerDestination.toBase64()
            myUuid = newMyUuid
            remoteUuid = newRemoteUuid
            startingPosition = null
            isArchived = false
        }
        dataRepository.addGame(game)
    }
}