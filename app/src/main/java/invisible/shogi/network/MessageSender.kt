/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import android.content.Context
import invisible.shogi.DataRepository
import invisible.shogi.db.entity.MessageEntity
import invisible.shogi.network.Common.RemoteDeclinedException
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import net.i2p.client.streaming.I2PSocket
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException

class MessageSender(private val message: MessageEntity, context: Context) : ConnectionHandler {
    private val dataRepository: DataRepository = DataRepository(context)

    @Throws(IOException::class)
    override suspend fun handle(socket: I2PSocket) = withContext(IO) {
        val dataInputStream = DataInputStream(socket.inputStream)
        val dataOutputStream = DataOutputStream(socket.outputStream)
        dataOutputStream.writeInt(ConnectionReceiver.MESSAGE_TYPE_MESSAGE)
        if (message.contactId == null && message.gameId != null) {
            val game = dataRepository.getGameById(message.gameId!!)
            dataOutputStream.writeBoolean(true) // forGame
            Common.writeUuid(game.remoteUuid!!, dataOutputStream)
        } else {
            dataOutputStream.writeBoolean(false) // not forGame
        }
        dataOutputStream.flush()
        val result = dataInputStream.readInt()
        if (result != Common.REPLY_ACK) {
            throw RemoteDeclinedException()
        }
        dataOutputStream.writeLong(message.instantMade!!.millis)
        dataOutputStream.writeUTF(message.content)
        dataOutputStream.flush()
    }
}