/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import android.content.Context
import invisible.shogi.DataRepository
import invisible.shogi.I2PUtil.destinationFromKey
import invisible.shogi.I2PUtil.generatePrivateKey
import invisible.shogi.db.entity.GameEntity
import invisible.shogi.model.GameMode
import invisible.shogi.model.Player
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import net.i2p.client.streaming.I2PSocket
import org.joda.time.Instant
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.util.*

internal class GameStartReceiver(context: Context) : ConnectionHandler {
    private val dataRepository: DataRepository = DataRepository(context)

    @Throws(IOException::class)
    override suspend fun handle(socket: I2PSocket) = withContext(IO) {
        val dataInput = DataInputStream(socket.inputStream)
        val dataOutput = DataOutputStream(socket.outputStream)
        val newRemoteUuid = Common.readUuid(dataInput)
        val newMyUuid = UUID.randomUUID()
        Common.writeUuid(newMyUuid, dataOutput)
        dataOutput.flush()

//        if (dataRepository.getGameByMyUuidNow(newRemoteUuid) != null) {
//            dataOutput.writeInt(REPLY_NACK_DONT_LIKE_UUID);
//            dataOutput.flush();
//            return;
//        }
        dataOutput.writeInt(Common.REPLY_ACK)
        dataOutput.flush()
        val newPrivateKey = generatePrivateKey()
        val keyId = dataRepository.savePrivateKey(newPrivateKey)
        val myDestinationBase64 = destinationFromKey(newPrivateKey).toBase64()
        dataOutput.writeUTF(myDestinationBase64)
        dataOutput.flush()
        val remoteDestinationBase64 = dataInput.readUTF()

        // TODO who's the starter? me!
        // TODO what's the starting position?
        val game = GameEntity().apply {
            ourPlayer = Player.BLACK
            mode = GameMode.I2P_DIRECT
            startedAt = Instant.now()
            name = dataRepository.getNextGameName()
            privateKeyId = keyId
            remoteDestination = remoteDestinationBase64
            myUuid = newMyUuid
            remoteUuid = newRemoteUuid
            startingPosition = null
            isArchived = false
        }
        dataRepository.addGame(game)
        Unit
    }
}