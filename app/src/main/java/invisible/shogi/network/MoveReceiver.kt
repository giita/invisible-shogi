/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import android.content.Context
import com.google.gson.Gson
import invisible.shogi.DataRepository
import invisible.shogi.logic.IllegalMoveException
import invisible.shogi.model.Game
import invisible.shogi.model.move.Move
import invisible.shogi.model.move.MoveFactory.makeMoveIn
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import net.i2p.client.streaming.I2PSocket
import org.joda.time.Instant
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException

internal class MoveReceiver(context: Context?) : ConnectionHandler {
    private val dataRepository: DataRepository = DataRepository(context!!)

    @Throws(IOException::class)
    override suspend fun handle(socket: I2PSocket) = withContext(IO) {
        val dataInput = DataInputStream(socket.inputStream)
        val dataOutput = DataOutputStream(socket.outputStream)
        val madeMillis = dataInput.readLong()
        val made = Instant(madeMillis)
        val gameMyUuid = Common.readUuid(dataInput)
        val moveJson = dataInput.readUTF()
        val move = Gson().fromJson(moveJson, NetworkedMove::class.java)
        dataRepository.moveAdditionLock.lock()
        try {
            val game: Game? = dataRepository.getGameByMyUuid(gameMyUuid)
            if (game == null || game.remoteDestination != socket.peerDestination.toBase64()) {
                dataOutput.writeInt(Common.REPLY_NACK_GAME_NOT_FOUND)
                dataOutput.flush()
                return@withContext
            }
            val moves: List<Move> = dataRepository.getMovesByGameId(game.id)
            try {
                // FIXME: this check is bogus and should be
                //  performed in dataRepository anyway
                game.realStartingPosition.applyInplace(moves)
            } catch (e: IllegalMoveException) {
                dataOutput.writeInt(Common.REPLY_NACK_ILLEGAL_MOVE)
                dataOutput.flush()
                return@withContext
            }
            dataOutput.writeInt(Common.REPLY_ACK)
            dataOutput.flush()
            dataRepository.saveMove(makeMoveIn(move, game.id, moves.size + 1, made, Instant.now()))
        } finally {
            dataRepository.moveAdditionLock.unlock()
        }
    }
}