/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.util.*

internal object Common {
    const val REPLY_ACK = 0
    const val REPLY_NACK_GAME_NOT_FOUND = 1
    const val REPLY_NACK_ILLEGAL_MOVE = 2
    /**
     * Currently unused since "not liking a UUID" is a security risk (leaks information).
     * Cases where this can happen were avoided.
     */
    const val REPLY_NACK_DONT_LIKE_UUID = 3
    const val REPLY_NACK_WRONG_UUID = 4
    const val REPLY_NACK_NOT_IN_CONTACT_LIST = 5

    @Throws(IOException::class)
    suspend fun readUuid(dataInput: DataInputStream): UUID = withContext(IO) {
        val p1: Long = dataInput.readLong()
        val p2: Long = dataInput.readLong()
        return@withContext UUID(p1, p2)
    }

    @Throws(IOException::class)
    suspend fun writeUuid(uuid: UUID, dataOutput: DataOutputStream) = withContext(IO) {
        dataOutput.writeLong(uuid.mostSignificantBits)
        dataOutput.writeLong(uuid.leastSignificantBits)
        dataOutput.flush()
    }

    class RemoteDeclinedException : RuntimeException()
}