/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import android.content.Context
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import net.i2p.I2PException
import net.i2p.client.streaming.I2PSocket
import net.i2p.client.streaming.I2PSocketManager
import net.i2p.data.DataFormatException
import net.i2p.data.Destination
import java.io.DataInputStream
import java.io.IOException

class CentralizedRandomMatchReceiver(private val context: Context, private val callback: Runnable) : NeedsManager {
    private lateinit var manager: I2PSocketManager
    @Throws(IOException::class, I2PException::class)
    override suspend fun handle(socket: I2PSocket) = withContext(IO) {
        val dataInputStream = DataInputStream(socket.inputStream)
        val type = dataInputStream.readInt()
        if (type != ConnectionReceiver.MATCH_MAKING_REPLY_YOUR_MATCH_FOLLOWS) {
            // it's START_GAME then
            GameStartReceiver(context).handle(socket)
            Handler(Looper.getMainLooper()).post(callback)
            return@withContext
        }
        val destinationString = dataInputStream.readUTF()
        var destination: Destination? = null
        try {
            destination = Destination(destinationString)
        } catch (e: DataFormatException) {
            // TODO
        }
        GameStartSender(context, MutableLiveData()).handle(manager.connect(destination))
        Handler(Looper.getMainLooper()).post(callback)
    }

    override fun setManager(manager: I2PSocketManager) {
        this.manager = manager
    }
}