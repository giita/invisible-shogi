/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import android.content.Context
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import net.i2p.client.streaming.I2PSocket
import java.io.DataInputStream
import java.io.IOException

class ConnectionReceiver(private val context: Context) : ConnectionHandler {
    @Throws(IOException::class)
    override suspend fun handle(socket: I2PSocket) = withContext(IO) {
        val dataInput = DataInputStream(socket.inputStream)
        when (dataInput.readInt()) {
            MESSAGE_TYPE_MOVE_MADE -> MoveReceiver(context).handle(socket)
            MESSAGE_TYPE_START_GAME -> GameStartReceiver(context).handle(socket)
            MESSAGE_TYPE_START_GAME_CONTACT -> GameStartReceiverForContact(context).handle(socket)
            MESSAGE_TYPE_MESSAGE -> MessageReceiver(context).handle(socket)
        }
        socket.close()
    }

    companion object {
        const val MESSAGE_TYPE_MOVE_MADE = 0
        const val MESSAGE_TYPE_START_GAME = 1
        const val MESSAGE_TYPE_START_GAME_CONTACT = 2
        const val MESSAGE_TYPE_MESSAGE = 3
        const val MATCH_MAKING_REPLY_YOUR_MATCH_FOLLOWS = 1002
    }
}