/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network.session

import invisible.shogi.network.ConnectionHandler
import net.i2p.client.streaming.I2PSocketManager

data class SessionInfo(
        var sessionId: Long,
        var name: String,
        var handler: ConnectionHandler,
        var keyId: Long = -1,
        var status: Int = 0,
        var exception: Exception? = null,
        var manager: I2PSocketManager? = null,
        var ephemeral: Boolean = keyId == -1L,
) {
    override fun toString(): String {
        return name + ": " + statusToString(status)
    }

    private fun statusToString(status: Int): String {
        when (status) {
            MANAGER_CREATION_FAILED -> return "manager creation failed"
            MANAGER_CREATED -> return "connecting session"
            CONNECTION_FINISHED -> return "connected"
            CONNECTION_FAILED -> return "session connection failed"
            SERVER_SOCKET_EXCEPTION -> return "error accepting incoming connections"
        }
        return "ok?"
    }

    companion object {
        const val MANAGER_CREATED = 2
        const val CONNECTION_FINISHED = 3
        const val CONNECTION_FAILED = 4
        const val MANAGER_CREATION_FAILED = 5
        const val SERVER_SOCKET_EXCEPTION = 6
    }
}