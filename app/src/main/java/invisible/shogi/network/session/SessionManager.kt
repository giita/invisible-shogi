/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network.session

import android.content.Context
import androidx.annotation.AnyThread
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import invisible.shogi.DataRepository
import invisible.shogi.InvisibleShogiApplication.Companion.appContext
import invisible.shogi.InvisibleShogiApplication.Companion.appScope
import invisible.shogi.R
import invisible.shogi.model.GameMode
import invisible.shogi.network.ConnectionHandler
import invisible.shogi.network.ConnectionReceiver
import invisible.shogi.network.NeedsManager
import invisible.shogi.network.session.SessionInfo.Companion.CONNECTION_FAILED
import invisible.shogi.network.session.SessionInfo.Companion.CONNECTION_FINISHED
import invisible.shogi.network.session.SessionInfo.Companion.MANAGER_CREATED
import invisible.shogi.network.session.SessionInfo.Companion.MANAGER_CREATION_FAILED
import invisible.shogi.network.session.SessionInfo.Companion.SERVER_SOCKET_EXCEPTION
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.i2p.client.I2PClient
import net.i2p.client.I2PSessionException
import net.i2p.client.streaming.I2PSocketManager
import net.i2p.client.streaming.I2PSocketManagerFactory
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

/** A singleton managing all the I2P sessions  */
class SessionManager(private val scope: CoroutineScope,
                     private val context: Context) {
    private val sessionInfoLiveDataBySessionId = LinkedHashMap<Long, MutableLiveData<SessionInfo>>()
    private val sessionIdByKeyId = HashMap<Long, Long>()

    private val ephemeralSessions
        get() = sessionInfoLiveDataBySessionId.values.asSequence().filter {
            it.value?.ephemeral == true
        }.map { it.value!! }.toList()
    private val keySessions
        get() = sessionInfoLiveDataBySessionId.values.asSequence().filter {
            it.value?.ephemeral == false
        }.map { it.value!! }.toList()

    private val _info = MutableLiveData("")
    val info: LiveData<String>
        get() = _info

    @AnyThread
    private fun updateInfo() {
        scope.launch(Main) {
            _info.value = getInformation()
        }
    }

    private suspend fun getInformation(): String = withContext(Default) {
        val information = StringBuilder()
        val ephemeralSessions = ephemeralSessions
        if (ephemeralSessions.isNotEmpty()) {
            information.append("Running ephemeral sessions:\n")
            for (info in ephemeralSessions) {
                information.append("  ").append(info).append('\n')
            }
        }
        if (sessionIdByKeyId.isNotEmpty()) {
            information.append("Running long-living sessions:\n")
            for (info in keySessions) {
                information.append("  ").append(info).append('\n')
            }
        }
        information.toString().trim { it <= ' ' }
    }

    @MainThread
    fun removeSession(sessionId: Long) {
        val sessionInfo = sessionInfoLiveDataBySessionId[sessionId]!!.value!!
        val manager = sessionInfo.manager
        if (manager != null) {
            scope.launch(IO) {
                manager.destroySocketManager()
            }
        }
        if (!sessionInfo.ephemeral) {
            sessionIdByKeyId.remove(sessionInfo.keyId)
        }
        sessionInfoLiveDataBySessionId.remove(sessionId)
        updateInfo()
    }

    fun getSessionInfoLive(sessionId: Long): LiveData<SessionInfo> {
        return sessionInfoLiveDataBySessionId[sessionId]!!
    }

    @MainThread
    fun getManagerForKeyId(keyId: Long): I2PSocketManager? {
        val info = sessionInfoLiveDataBySessionId[sessionIdByKeyId[keyId]]?.value
        return info?.manager
    }

    @MainThread
    fun getManager(sessionId: Long): I2PSocketManager? {
        val info = sessionInfoLiveDataBySessionId[sessionId]?.value
        return info?.manager
    }

    @AnyThread
    fun startSessions() {
        scope.launch {
            val repository = DataRepository(context)
            val primaryKeyId = repository.primaryKeyId
            // TODO: call getManager on main thread
            if (primaryKeyId >= 0 && getManagerForKeyId(primaryKeyId) == null) {
                newSession("Primary session", ConnectionReceiver(context), primaryKeyId)
            }
            val games = repository.getGamesByMode(GameMode.I2P_DIRECT)
            for (game in games) {
                if (getManagerForKeyId(game.privateKeyId) != null) {
                    continue
                }
                val gameName = game.name
                newSession(gameName
                        ?: "???", ConnectionReceiver(context), game.privateKeyId)
            }
        }
    }

    @AnyThread
    fun stopSessions() {
        scope.launch(Main) {
            for (info in ephemeralSessions) {
                if (info.manager != null) { // FIXME: do it on other thread
                    info.manager!!.destroySocketManager()
                }
            }
            for (info in keySessions) {
                if (info.manager != null) {
                    info.manager!!.destroySocketManager()
                }
            }
            sessionIdByKeyId.clear()
            sessionInfoLiveDataBySessionId.clear()
            updateInfo()
        }
    }

    private suspend fun initiateSession(infoLive: MutableLiveData<SessionInfo>, keyStream: InputStream?): Boolean = withContext(IO) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val sessionNickname = context.getString(R.string.tunnel_nickname) + ": " + infoLive.value!!.name
        val sessionProperties = Properties().apply {
            setProperty("inbound.nickname", sessionNickname)
            setProperty("outbound.nickname", sessionNickname)
            setProperty(I2PClient.PROP_SIGTYPE, "RedDSA_SHA512_Ed25519")
        }
        val host = prefs.getString("i2cp_address", null)
        val portString = prefs.getString("i2cp_port", "0")
        val port = portString?.toIntOrNull() ?: 0
        val manager = try {
            I2PSocketManagerFactory.createDisconnectedManager(keyStream, host, port, sessionProperties)
        } catch (e: I2PSessionException) {
            scope.launch(Main) {
                infoLive.value = infoLive.value?.copy(
                        exception = e,
                        status = MANAGER_CREATION_FAILED
                )
                updateInfo()
            }
            return@withContext false
        }
        scope.launch(Main) {
            infoLive.value = infoLive.value?.copy(
                    manager = manager,
                    status = MANAGER_CREATED
            )
            if (infoLive.value!!.handler is NeedsManager) {
                (infoLive.value!!.handler as NeedsManager).setManager(manager)
            }
            updateInfo()
        }
        val session = manager.session
        try {
            session.connect()
        } catch (e: I2PSessionException) {
            scope.launch(Main) {
                infoLive.value = infoLive.value?.copy(
                        exception = e,
                        status = CONNECTION_FAILED
                )
                updateInfo()
            }
            return@withContext false
        }
        scope.launch(Main) {
            infoLive.value = infoLive.value?.copy(
                    status = CONNECTION_FINISHED
            )
            updateInfo()
        }
        return@withContext true
    }

    private suspend fun runSession(infoLive: MutableLiveData<SessionInfo>, key: InputStream?) {
        if (!initiateSession(infoLive, key)) {
            return
        }
        try {
            val serverSocket = infoLive.value!!.manager?.serverSocket!!
            withContext(IO) {
                while (true) {
                    val socket = serverSocket.accept()
                    infoLive.value!!.handler.handle(socket)
                }
            }
        } catch (e: Exception) {
            scope.launch(Main) {
                infoLive.value = infoLive.value?.copy(
                        exception = e,
                        status = SERVER_SOCKET_EXCEPTION
                )
            }
        }
    }

    @MainThread
    fun newSession(name: String, incomingHandler: ConnectionHandler, keyId: Long = -1): Long {
        if (sessionIdByKeyId.containsKey(keyId)) {
            throw IllegalArgumentException("Session already running for this key")
        }
        val info = SessionInfo(getNewSessionId(), name, incomingHandler, keyId)
        val infoLive = MutableLiveData(info)
        sessionInfoLiveDataBySessionId[info.sessionId] = infoLive
        if (keyId == -1L) {
            scope.launch {
                runSession(infoLive, key = null)
            }
        } else {
            sessionIdByKeyId[keyId] = info.sessionId
            scope.launch(Main) {
                val key = ByteArrayInputStream(DataRepository(context).getPrivateKey(keyId))
                scope.launch {
                    runSession(infoLive, key)
                }
            }
        }
        return info.sessionId
    }

    private var nextSessionId: Long = 0

    @MainThread
    private fun getNewSessionId(): Long {
        val sessionId = nextSessionId
        nextSessionId += 1
        return sessionId
    }

    companion object {
        val sessionManager = SessionManager(appScope, appContext)
    }
}