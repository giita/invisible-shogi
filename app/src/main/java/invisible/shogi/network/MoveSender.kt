/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import android.content.Context
import com.google.gson.Gson
import invisible.shogi.DataRepository
import invisible.shogi.model.Game
import invisible.shogi.model.move.GameMove
import invisible.shogi.network.Common.RemoteDeclinedException
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import net.i2p.client.streaming.I2PSocket
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.io.OutputStreamWriter

class MoveSender(private val move: GameMove, context: Context) : ConnectionHandler {
    private val dataRepository: DataRepository = DataRepository(context)

    @Throws(IOException::class)
    override suspend fun handle(socket: I2PSocket) = withContext(IO) {
        val dataInput = DataInputStream(socket.inputStream)
        val writer = OutputStreamWriter(socket.outputStream)
        val dataOutput = DataOutputStream(socket.outputStream)
        dataOutput.writeInt(ConnectionReceiver.MESSAGE_TYPE_MOVE_MADE)
        dataOutput.writeLong(move.instantMade!!.millis)
        val game: Game? = dataRepository.getGameById(move.gameId)
        Common.writeUuid(game!!.remoteUuid!!, dataOutput)
        val a = Gson().toJson(NetworkedMove(move))
        dataOutput.writeUTF(a)
        writer.flush()
        val result = dataInput.readInt()
        if (result != Common.REPLY_ACK) {
            throw RemoteDeclinedException()
        }
    }
}