/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.network

import android.content.Context
import invisible.shogi.DataRepository
import invisible.shogi.db.entity.ContactEntity
import invisible.shogi.db.entity.GameEntity
import invisible.shogi.db.entity.MessageEntity
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import net.i2p.client.streaming.I2PSocket
import org.joda.time.Instant
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException

class MessageReceiver(context: Context) : ConnectionHandler {
    private val dataRepository: DataRepository = DataRepository(context)

    @Throws(IOException::class)
    override suspend fun handle(socket: I2PSocket) = withContext(IO) {
        val dataInputStream = DataInputStream(socket.inputStream)
        val dataOutputStream = DataOutputStream(socket.outputStream)
        val forGame = dataInputStream.readBoolean()
        var game: GameEntity? = null
        var contact: ContactEntity? = null
        if (forGame) {
            val gameMyUuid = Common.readUuid(dataInputStream)
            game = dataRepository.getGameByMyUuid(gameMyUuid)
            if (game == null || game.remoteDestination != socket.peerDestination.toBase64()) {
                dataOutputStream.writeInt(Common.REPLY_NACK_WRONG_UUID)
                dataOutputStream.flush()
                return@withContext
            }
        } else {
            contact = dataRepository.getContactByDestination(socket.peerDestination.toBase64())
            if (contact == null) {
                dataOutputStream.writeInt(Common.REPLY_NACK_NOT_IN_CONTACT_LIST)
                dataOutputStream.flush()
                return@withContext
            }
        }
        dataOutputStream.writeInt(Common.REPLY_ACK)
        dataOutputStream.flush()
        val instantMadeMillis = dataInputStream.readLong()
        val messageContent = dataInputStream.readUTF()
        val message = MessageEntity().apply {
            byUs = false
            gameId = game?.id
            contactId = contact?.id
            instantMade = Instant(instantMadeMillis)
            instantReceived = Instant.now()
            content = messageContent
        }
        dataRepository.addMessage(message)
    }
}