/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.logic

import androidx.annotation.WorkerThread
import invisible.shogi.logic.Position.Companion.BOARD_SIZE
import invisible.shogi.model.Coordinates
import invisible.shogi.model.PieceType
import invisible.shogi.model.Player
import invisible.shogi.model.move.Move
import invisible.shogi.model.move.MoveFactory.finishMove
import invisible.shogi.model.move.MoveFactory.startDrop
import invisible.shogi.model.move.MoveFactory.startMove
import invisible.shogi.model.move.StartedMove
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.withContext
import kotlin.math.abs

typealias PossibleMoves = Array<out Array<out List<Move>>>
typealias MutablePossibleMoves = Array<Array<MutableList<Move>>>

enum class CheckLevel {
    NO_CHECKS,
    CHECK_SANITY_ONLY,
    FULL_CHECK
}

/** Checks a move for validity in the given position. Has some convenience static methods.  */
class MoveChecker(private val position: Position, private val move: Move) {
    @Throws(IllegalMoveException::class)
    fun checkIsCorrect(level: CheckLevel = CheckLevel.FULL_CHECK) {
        when (level) {
            CheckLevel.NO_CHECKS -> {
            }
            CheckLevel.CHECK_SANITY_ONLY -> checkIsSane()
            CheckLevel.FULL_CHECK -> checkIsTotallyCorrect()
        }
    }

    @Throws(IllegalMoveException::class)
    private fun checkIsSane() {
        assertTrue(move.pieceCoordinatesAfter.inBounds(BOARD_SIZE), "Target coordinates should be on board")
        if (move.isDrop) {
            assertTrue(position.getPiecesOnHand(position.playerToMove).contains(move.piece))
        } else {
            assertTrue(move.piece == position.getPieceAt(move.requirePieceCoordinatesBefore()), "Piece to move should be equal to piece on source position")
            assertTrue(move.requirePieceCoordinatesBefore().inBounds(BOARD_SIZE), "Source coordinates should be on board")
        }
        if (move.isPromoting) {
            assertTrue(move.pieceType.canBePromoted, "Piece type should be able to be promoted")
        }
    }

    @Throws(IllegalMoveException::class)
    private fun checkIsTotallyCorrect() {
        checkIsSane()
        checkNoForbiddenCapturing()
        checkNeedPromoting()
        checkCanPromote()
        checkPieceMoves()
        checkNoObstacles()
        checkTwoPawns()
        checkNoThreatToKing()

        // should be the last thing to check
        checkNoCheckmateAfterPawnDrop()
    }

    @Throws(IllegalMoveException::class)
    private fun checkNoCheckmateAfterPawnDrop() {
        if (move.isDrop && move.pieceType == PieceType.PAWN) {
            val clonedPosition: Position = position.clone().applyInplace(move)
            // TODO: too expensive? or recursion?
            assertTrue(!isCheckmate(clonedPosition, false), "Checkmate in one move by dropping a fuhyou is forbidden")
        }
    }

    @Throws(IllegalMoveException::class)
    private fun checkCanPromote() {
        if (move.isPromoting) {
            assertTrue(if (position.playerToMove == Player.BLACK) move.pieceCoordinatesAfter.row <= 2 || move.requirePieceCoordinatesBefore().row <= 2 else move.pieceCoordinatesAfter.row >= BOARD_SIZE - 1 - 2 || move.requirePieceCoordinatesBefore().row >= BOARD_SIZE - 1 - 2,
                    "Should be in or enter promotion zone to promote")
        }
    }

    @Throws(IllegalMoveException::class)
    private fun checkNeedPromoting() {
        if (move.pieceType == PieceType.KNIGHT && !move.isPromoting) {
            assertTrue(if (position.playerToMove == Player.BLACK) move.pieceCoordinatesAfter.row >= 2 else move.pieceCoordinatesAfter.row <= BOARD_SIZE - 1 - 2, "Should promote keima if in last two rows")
        }
        if ((move.pieceType == PieceType.PAWN || move.pieceType == PieceType.LANCE) && !move.isPromoting) {
            assertTrue(if (position.playerToMove == Player.BLACK) move.pieceCoordinatesAfter.row >= 1 else move.pieceCoordinatesAfter.row <= BOARD_SIZE - 1 - 1, "Should promote fuhyou and kyousha if in the last row")
        }
    }

    @Throws(IllegalMoveException::class)
    private fun checkNoForbiddenCapturing() {
        val pieceToCapture = position.getPieceAt(move.pieceCoordinatesAfter)
        if (pieceToCapture != null) {
            assertTrue(pieceToCapture.player != position.playerToMove, "Capturing own pieces is forbidden")
        }
        if (move.isDrop) {
            assertTrue(position.getPieceAt(move.pieceCoordinatesAfter) == null, "Capturing while dropping is forbidden")
            assertTrue(!move.isPromoting, "Dropping with promotion is forbidden")
        }
    }

    @Throws(IllegalMoveException::class)
    private fun checkNoThreatToKing() {
        val pieceToCapture = position.getPieceAt(move.pieceCoordinatesAfter)
        if (pieceToCapture == null || pieceToCapture.type != PieceType.KING) {
            val clonedPositionForCheck: Position = position.clone().applyInplace(move)
            assertTrue(!isCheck(clonedPositionForCheck), "The move should not threaten the king")
        }
    }

    @Throws(IllegalMoveException::class)
    private fun checkNoObstacles() {
        if (move.isDrop || move.pieceType == PieceType.KNIGHT) {
            return
        }
        val start = move.requirePieceCoordinatesBefore()
        val end = move.pieceCoordinatesAfter
        val horizontalDiff = end.column - start.column
        val horizontalStep = direction(horizontalDiff)
        val verticalDiff = end.row - start.row
        val verticalStep = direction(verticalDiff)
        if (verticalDiff != 0 && horizontalDiff != 0 && abs(verticalDiff) != abs(horizontalDiff))
            throw AssertionError()
        var currentRow = start.row + verticalStep
        var currentColumn = start.column + horizontalStep
        while (currentRow != end.row || currentColumn != end.column) {
            assertTrue(position.getPieceAt(Coordinates(currentRow, currentColumn)) == null, "There should be no pieces on the way")
            currentRow += verticalStep
            currentColumn += horizontalStep
        }
    }

    private fun direction(diff: Int): Int {
        return if (diff < 0) -1 else if (diff > 0) 1 else 0
    }

    @Throws(IllegalMoveException::class)
    private fun checkPieceMoves() {
        if (move.isDrop) {
            return
        }
        val before = move.requirePieceCoordinatesBefore()
        val after = move.pieceCoordinatesAfter
        var dx = before.column - after.column
        var dy = before.row - after.row
        if (position.playerToMove == Player.WHITE) {
            dx = -dx
            dy = -dy
        }
        val reason = "Move rules for the piece"
        when (move.pieceType) {
            /* .....
               ..o..
               ..#..
               .....
             */
            PieceType.PAWN ->
                assertTrue(dx == 0 && dy == 1, reason)
            /* .....
               .ooo.
               ..#..
               .o.o.
               .....
             */
            PieceType.SILVER ->
                assertTrue(dy == 1 && abs(dx) <= 1 || dy == -1 && abs(dx) == 1, reason)
            /* .....
               .ooo.
               .o#o.
               .ooo.
               .....
             */
            PieceType.KING ->
                assertTrue(abs(dx) <= 1 && abs(dy) <= 1, reason)
            /* ..|..
               ..|..
               --#--
               ..|..
               ..|..
             */
            PieceType.ROOK ->
                assertTrue(dx == 0 || dy == 0, reason)
            /* \.../
               .\./.
               ..#..
               ./.\.
               /...\
             */
            PieceType.BISHOP ->
                assertTrue(abs(dx) == abs(dy), reason)
            /* .....
               .*.*.
               .....
               ..#..
               .....
             */
            PieceType.KNIGHT ->
                assertTrue(dy == 2 && (dx == -1 || dx == 1), reason)
            /* ..|..
               ..|..
               ..|..
               ..#..
               .....
             */
            PieceType.LANCE ->
                assertTrue(dy >= 1 && dx == 0, reason)
            /* \.../
               .\o/.
               .o#o.
               ./o\.
               /...\
             */
            PieceType.PROMOTED_BISHOP ->
                assertTrue(abs(dx) == abs(dy) || abs(dx) <= 1 && abs(dy) <= 1, reason)
            /* ..|..
               .o|o.
               --#--
               .o|o.
               ..|..
             */
            PieceType.PROMOTED_ROOK ->
                assertTrue(dx == 0 || dy == 0 || abs(dx) <= 1 && abs(dy) <= 1, reason)
            /* .....
               .ooo.
               .o#o.
               ..o..
               .....
             */
            PieceType.GOLD,
            PieceType.PROMOTED_SILVER,
            PieceType.PROMOTED_KNIGHT,
            PieceType.PROMOTED_LANCE,
            PieceType.PROMOTED_PAWN ->
                assertTrue(dy <= 1 && (dx == 0 && dy >= -1 || abs(dx) == 1 && dy >= 0), reason)
        }
    }

    @Throws(IllegalMoveException::class)
    private fun checkTwoPawns() {
        if (!move.isDrop || move.pieceType != PieceType.PAWN) {
            return
        }
        for (i in 0 until BOARD_SIZE) {
            val piece = position.getPieceAt(Coordinates(i, move.pieceCoordinatesAfter.column))
            assertTrue(piece == null || piece.player != move.player || piece.type != PieceType.PAWN, "Two pawns rule")
        }
    }

    companion object {
        @Throws(IllegalMoveException::class)
        private fun assertTrue(condition: Boolean, whyShouldBeTrue: String = "") {
            if (!condition) {
                throw IllegalMoveException(whyShouldBeTrue)
            }
        }

        @WorkerThread
        private fun isLegalMove(position: Position, move: Move): Boolean {
            var legal = true
            try {
                MoveChecker(position, move).checkIsCorrect()
            } catch (e: IllegalMoveException) {
                legal = false
            }
            return legal
        }

        suspend fun isLegalMoveSuspend(position: Position, move: Move): Boolean {
            return withContext(Default) {
                isLegalMove(position, move)
            }
        }

        private fun getPossibleMoveEndings(position: Position, startedMove: StartedMove): List<Move> {
            val possibleMoveEndings = ArrayList<Move>()
            for (i in 0 until BOARD_SIZE) {
                for (j in 0 until BOARD_SIZE) {
                    var newMove = finishMove(startedMove, Coordinates(i, j), false)
                    if (isLegalMove(position, newMove)) {
                        possibleMoveEndings.add(newMove)
                    }
                    newMove = finishMove(startedMove, Coordinates(i, j), true)
                    if (isLegalMove(position, newMove)) {
                        possibleMoveEndings.add(newMove)
                    }
                }
            }
            return possibleMoveEndings
        }

        fun noPossibleMoves(): MutablePossibleMoves {
            return Array(BOARD_SIZE) { Array(BOARD_SIZE) { arrayListOf() } }
        }

        @WorkerThread
        fun getPossibleMoveEndingsByCoordinates(position: Position, startedMove: StartedMove): MutablePossibleMoves {
            val possibleMoves = noPossibleMoves()
            for (move in getPossibleMoveEndings(position, startedMove)) {
                val to = move.pieceCoordinatesAfter
                possibleMoves[to.row][to.column].add(move)
            }
            return possibleMoves
        }

        suspend fun getPossibleMoveEndingsByCoordinatesSuspend(position: Position, startedMove: StartedMove): MutablePossibleMoves {
            return withContext(Default) {
                getPossibleMoveEndingsByCoordinates(position, startedMove)
            }
        }

        /**
         * Checks whether the current player threatens the opposite king. Since that does not actually
         * happen in real positions, this method is only used to check that a move does not result in
         * such a situation.
         */
        private fun isCheck(position: Position): Boolean {
            var ourKingCoordinates: Coordinates? = null
            run {
                var i = 0
                while (i < BOARD_SIZE && ourKingCoordinates == null) {
                    for (j in 0 until BOARD_SIZE) {
                        val start = Coordinates(i, j)
                        val piece = position.getPieceAt(start)
                        if (piece != null && piece.player != position.playerToMove && piece.type == PieceType.KING) {
                            ourKingCoordinates = start
                            break
                        }
                    }
                    ++i
                }
            }
            if (ourKingCoordinates == null) {
                return false
            }
            for (i in 0 until BOARD_SIZE) {
                for (j in 0 until BOARD_SIZE) {
                    val start = Coordinates(i, j)
                    val piece = position.getPieceAt(start)
                    if (piece != null && piece.player == position.playerToMove) {
                        val startedMove = startMove(piece, start)
                        if (isLegalMove(position, finishMove(startedMove, ourKingCoordinates!!, false))
                                || isLegalMove(position, finishMove(startedMove, ourKingCoordinates!!, true))) {
                            return true
                        }
                    }
                }
            }
            return false
        }

        /**
         * Checks that the current player has no moves. Optionally does not consider drops in cases
         * the caller is sure they are not possible (since checks for them can recurse because of the
         * the pawn drop checkmate rule).
         */
        private fun isCheckmate(position: Position, allowDrops: Boolean): Boolean {
            if (allowDrops) {
                for (piece in position.getPiecesOnHand(position.playerToMove)) {
                    if (getPossibleMoveEndings(position, startDrop(piece)).isNotEmpty()) {
                        return false
                    }
                }
            }
            for (i in 0 until BOARD_SIZE) {
                for (j in 0 until BOARD_SIZE) {
                    val start = Coordinates(i, j)
                    val piece = position.getPieceAt(start)
                    if (piece == null || piece.player != position.playerToMove) {
                        continue
                    }
                    if (getPossibleMoveEndings(position, startMove(piece, start)).isNotEmpty()) {
                        return false
                    }
                }
            }
            return true
        }

        /** Checks that the current player has no moves.  */
        fun isCheckmate(position: Position): Boolean {
            return isCheckmate(position, true)
        }
    }
}