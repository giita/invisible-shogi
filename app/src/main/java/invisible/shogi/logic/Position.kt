/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.logic

import androidx.annotation.WorkerThread
import invisible.shogi.model.Coordinates
import invisible.shogi.model.Piece
import invisible.shogi.model.PieceType
import invisible.shogi.model.Player
import invisible.shogi.model.move.Move
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.withContext

/**
 * One position in the game. Immutable, cloneable. Has some package-private modification methods
 * that can break the invariant of the position being correct (for testing purposes), but otherwise
 * should always be correct.
 */
class Position : Cloneable {
    private var hands: MutableList<Piece> = ArrayList()
    var currentMoveNumber: Int = 1

    var playerToMove: Player
        get() = if (currentMoveNumber % 2 == 1) { Player.BLACK } else { Player.WHITE }
        set(p) {
            val c = if (p == Player.BLACK) { 1 } else { 2 }
            if (currentMoveNumber % 2 != c % 2) currentMoveNumber = c
        }

    private var board = arrayOf(
            arrayOf<Piece?>(
                    Piece(Player.WHITE, PieceType.LANCE),
                    Piece(Player.WHITE, PieceType.KNIGHT),
                    Piece(Player.WHITE, PieceType.SILVER),
                    Piece(Player.WHITE, PieceType.GOLD),
                    Piece(Player.WHITE, PieceType.KING),
                    Piece(Player.WHITE, PieceType.GOLD),
                    Piece(Player.WHITE, PieceType.SILVER),
                    Piece(Player.WHITE, PieceType.KNIGHT),
                    Piece(Player.WHITE, PieceType.LANCE)),
            arrayOf(
                    null, Piece(Player.WHITE, PieceType.ROOK), null,
                    null, null, null,
                    null, Piece(Player.WHITE, PieceType.BISHOP), null),
            arrayOf<Piece?>(
                    Piece(Player.WHITE, PieceType.PAWN),
                    Piece(Player.WHITE, PieceType.PAWN),
                    Piece(Player.WHITE, PieceType.PAWN),
                    Piece(Player.WHITE, PieceType.PAWN),
                    Piece(Player.WHITE, PieceType.PAWN),
                    Piece(Player.WHITE, PieceType.PAWN),
                    Piece(Player.WHITE, PieceType.PAWN),
                    Piece(Player.WHITE, PieceType.PAWN),
                    Piece(Player.WHITE, PieceType.PAWN)),

            arrayOf<Piece?>(null, null, null, null, null, null, null, null, null),
            arrayOf<Piece?>(null, null, null, null, null, null, null, null, null),
            arrayOf<Piece?>(null, null, null, null, null, null, null, null, null),

            arrayOf<Piece?>(
                    Piece(Player.BLACK, PieceType.PAWN),
                    Piece(Player.BLACK, PieceType.PAWN),
                    Piece(Player.BLACK, PieceType.PAWN),
                    Piece(Player.BLACK, PieceType.PAWN),
                    Piece(Player.BLACK, PieceType.PAWN),
                    Piece(Player.BLACK, PieceType.PAWN),
                    Piece(Player.BLACK, PieceType.PAWN),
                    Piece(Player.BLACK, PieceType.PAWN),
                    Piece(Player.BLACK, PieceType.PAWN)),
            arrayOf(
                    null, Piece(Player.BLACK, PieceType.BISHOP), null,
                    null, null, null,
                    null, Piece(Player.BLACK, PieceType.ROOK), null),
            arrayOf<Piece?>(
                    Piece(Player.BLACK, PieceType.LANCE),
                    Piece(Player.BLACK, PieceType.KNIGHT),
                    Piece(Player.BLACK, PieceType.SILVER),
                    Piece(Player.BLACK, PieceType.GOLD),
                    Piece(Player.BLACK, PieceType.KING),
                    Piece(Player.BLACK, PieceType.GOLD),
                    Piece(Player.BLACK, PieceType.SILVER),
                    Piece(Player.BLACK, PieceType.KNIGHT),
                    Piece(Player.BLACK, PieceType.LANCE)))

    constructor()

    internal constructor(board: Array<Array<Piece?>>) {
        this.board = board
    }

    fun setHands(list: MutableList<Piece>) {
        hands = list
    }

    fun getPieceAt(coordinates: Coordinates): Piece? {
        return board[coordinates.row][coordinates.column]
    }

    private fun setPieceAt(coordinates: Coordinates, piece: Piece?) {
        board[coordinates.row][coordinates.column] = piece
    }

    @WorkerThread
    @Throws(IllegalMoveException::class)
    fun applyInplace(move: Move): Position {
        MoveChecker(this, move).checkIsCorrect(CheckLevel.CHECK_SANITY_ONLY)
        if (move.isDrop) {
            hands.remove(move.piece)
        } else {
            setPieceAt(move.requirePieceCoordinatesBefore(), null)
        }
        if (getPieceAt(move.pieceCoordinatesAfter) != null) {
            var pieceTypeToCapture = getPieceAt(move.pieceCoordinatesAfter)!!.type
            if (pieceTypeToCapture.isPromoted) {
                pieceTypeToCapture = pieceTypeToCapture.unpromoted
            }
            hands.add(Piece(playerToMove, pieceTypeToCapture))
        }
        var pieceToPlace = move.piece
        if (move.isPromoting) {
            pieceToPlace = Piece(pieceToPlace.player, pieceToPlace.type.promoted)
        }
        setPieceAt(move.pieceCoordinatesAfter, pieceToPlace)
        ++currentMoveNumber
        return this
    }

    // TODO: make this suspend and withContext(Default)
    @Throws(IllegalMoveException::class)
    suspend fun applyInplace(moves: List<Move>): Position {
        withContext(Default) {
            moves.forEach { applyInplace(it) }
        }
        return this
    }

    fun getPiecesOnHand(player: Player): List<Piece> {
        return hands.asSequence().filter { it.player == player }.toList()
    }

    @Throws(CloneNotSupportedException::class)
    public override fun clone(): Position {
        val result = super.clone() as Position
        result.hands = ArrayList(hands)
        result.board = board.clone()
        for (i in board.indices) {
            result.board[i] = board[i].clone()
        }
        return result
    }

    companion object {
        const val BOARD_SIZE = 9
    }
}