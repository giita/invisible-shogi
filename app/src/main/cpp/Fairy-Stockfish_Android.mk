LOCAL_PATH := $(call my-dir)

rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))
SF_IGNORED_FILES := ffishjs.cpp pyffish.cpp
SF_SRC_FILES_ALL := $(subst $(LOCAL_PATH)/,,$(call rwildcard,$(LOCAL_PATH),*.cpp))
SF_SRC_FILES := $(filter-out $(SF_IGNORED_FILES),$(SF_SRC_FILES_ALL))

MY_ARCH_DEF :=
ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
  MY_ARCH_DEF += -DIS_64BIT -DUSE_POPCNT
endif
ifeq ($(TARGET_ARCH_ABI),x86_64)
  MY_ARCH_DEF += -DIS_64BIT
endif

include $(CLEAR_VARS)
LOCAL_MODULE    := stockfish
LOCAL_SRC_FILES := $(SF_SRC_FILES)
LOCAL_CFLAGS    := -std=c++17 -O2 -fPIE $(MY_ARCH_DEF) -s -frtti \
                   -DLARGEBOARDS -DPRECOMPUTED_MAGICS -DNNUE_EMBEDDING_OFF
LOCAL_LDFLAGS	+= -fPIE -pie -s
include $(BUILD_EXECUTABLE)
