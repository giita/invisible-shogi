/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.logic

import invisible.shogi.model.Coordinates
import org.junit.jupiter.api.Test

internal class MoveCheckerMiscellaneousTest {
    @Test
    fun testPromotedRookObstacleOur() {
        val expectedMoves = """
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | |W|W|W| | | |
            | | | | | |W|W|W|W|
            | | | |W|W|W| | | |
            | | | | |W| | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | ^p^R| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 4))
    }

    @Test
    fun testPromotedRookObstacleTheir() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | |W| | | | |
            | | | |W|W|W| | | |
            |W|W|W|W| |W|W|W|W|
            | | | |W|W|W| | | |
            | | | | |W| | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | |p| | | | |
            | | | | | | | | | |
            | | | | ^R| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 4))
    }

    @Test
    fun testMovesUnderCheck() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | |W| |W| | | |
            | | | | | | | | | |
            | | | |W|W|W| | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | |p| | | | |
            | | | | |b| | | | |
            | | | | ^K| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 4))
    }

    @Test
    fun testShouldPreventCheck() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | |W| | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | |l| | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | ^r| | | | | | | |
            | | | | | | | | | |
            | | | | ^K| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(4, 1))
    }

    @Test
    fun testShouldPreventCheckAndPromote() {
        val expectedMoves = """
            | | | | |P| | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | ^K| | | | |r| | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | ^l| | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(3, 4))
    }
}