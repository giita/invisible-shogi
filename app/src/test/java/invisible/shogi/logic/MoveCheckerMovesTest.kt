/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.logic

import invisible.shogi.model.Coordinates
import org.junit.jupiter.api.Test

internal class MoveCheckerMovesTest {
    @Test
    fun testPawnMove() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | |W| | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | ^p| | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 1))
    }

    @Test
    fun testKnightMove() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | |W| |W| | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | ^n| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 4))
    }

    @Test
    fun testRookMove() {
        val expectedMoves = """
            | | | | |A| | | | |
            | | | | |A| | | | |
            | | | | |A| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            |W|W|W|W| |W|W|W|W|
            | | | | |W| | | | |
            | | | | |W| | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | ^r| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 4))
    }

    @Test
    fun testBishopMove() {
        val expectedMoves = """
            | | | | | | | | | |
            |A| | | | | | | | |
            | |A| | | | | | | |
            | | |W| | | | | |W|
            | | | |W| | | |W| |
            | | | | |W| |W| | |
            | | | | | | | | | |
            | | | | |W| |W| | |
            | | | |W| | | |W| |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | ^b| | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 5))
    }

    @Test
    fun testPromotedBishopMove() {
        val expectedMoves = """
            | | | | | | | | | |
            |W| | | | | | | | |
            | |W| | | | | | | |
            | | |W| | | | | |W|
            | | | |W| | | |W| |
            | | | | |W|W|W| | |
            | | | | |W| |W| | |
            | | | | |W|W|W| | |
            | | | |W| | | |W| |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | ^B| | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 5))
    }

    @Test
    fun testPromotedRookMove() {
        val expectedMoves = """
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | |W|W|W| | | |
            |W|W|W|W| |W|W|W|W|
            | | | |W|W|W| | | |
            | | | | |W| | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | ^R| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 4))
    }

    @Test
    fun testSilverMove() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | |W|W|W| | | |
            | | | | | | | | | |
            | | | |W| |W| | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | ^s| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 4))
    }

    @Test
    fun testGoldLikeMoves() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | |W|W|W| | | |
            | | | |W| |W| | | |
            | | | | |W| | | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | ^*| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        for (p in "GSNLP".toCharArray()) {
            checkPossibleFinishes(expectedMoves, position.replace('*', p), Coordinates(6, 4))
        }
    }

    @Test
    fun testGoldLikeMovesWhite() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | |W| | | | |
            | | | |W| |W| | | |
            | | | |W|W|W| | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | |*| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        for (p in "GSNLP".toCharArray()) {
            checkPossibleFinishes(expectedMoves, position.replace('*', p), Coordinates(6, 4))
        }
    }

    @Test
    fun testLanceMoves() {
        val expectedMoves = """
            | | | | |P| | | | |
            | | | | |A| | | | |
            | | | | |A| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | | |W| | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | ^l| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 4))
    }

    @Test
    fun testKingMove() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | |W|W|W| | | |
            | | | |W| |W| | | |
            | | | |W|W|W| | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | ^K| | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishes(expectedMoves, position, Coordinates(6, 4))
    }
}