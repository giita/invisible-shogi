/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.logic

import invisible.shogi.model.Piece
import invisible.shogi.model.PieceType
import invisible.shogi.model.Player
import org.junit.jupiter.api.Test

internal class MoveCheckerDropTest {
    @Test
    fun testPawnDrop() {
        val expectedMoves = """
            | | | | | | | | | |
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|

            """.trimIndent()
        val position = """
            |K| | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishesDrop(expectedMoves, position, Piece(Player.BLACK, PieceType.PAWN))
    }

    @Test
    fun testPawnDropTwoPawns() {
        val expectedMoves = """
            | | | | | | | | | |
            |W|W| |W| |W|W|W|W|
            |W|W| |W| |W|W|W|W|
            |W|W| |W| |W|W|W|W|
            |W| | |W| |W| | |W|
            |W|W| |W| |W|W|W|W|
            |W|W| |W| |W|W|W|W|
            |W|W| |W| |W|W|W|W|
            |W|W| |W| |W|W|W|W|

            """.trimIndent()
        val position = """
            |K| | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | ^P| | | | |p|P| |
            | | | | | | | | | |
            | | ^p| | | | | | |
            | | | | ^p| | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishesDrop(expectedMoves, position, Piece(Player.BLACK, PieceType.PAWN))
    }

    @Test
    fun testPawnDropNoCheckmate() {
        val expectedMoves = """
            | | | | | | | | | |
            | |W|W|W| |W|W|W|W|
            |W|W|W| |W| |W|W|W|
            |W|W|W|W| |W|W|W|W|
            |W|W|W|W| |W|W|W|W|
            |W|W|W| | | |W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            |p| | | ^B| | | | |
            | | | ^P| ^P| | | |
            | | | | |K| | | | |
            | | | | | | | | | |
            | | | ^P^P^P| | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishesDrop(expectedMoves, position, Piece(Player.BLACK, PieceType.PAWN))
    }

    @Test
    fun testKnightDrop() {
        val expectedMoves = """
            | | | | | | | | | |
            | | | | | | | | | |
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishesDrop(expectedMoves, position, Piece(Player.BLACK, PieceType.KNIGHT))
    }

    @Test
    fun testKnightDropWhite() {
        val expectedMoves = """
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            |W|W|W|W|W|W|W|W|W|
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        val position = """
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |
            | | | | | | | | | |

            """.trimIndent()
        checkPossibleFinishesDrop(expectedMoves, position, Piece(Player.WHITE, PieceType.KNIGHT))
    }
}