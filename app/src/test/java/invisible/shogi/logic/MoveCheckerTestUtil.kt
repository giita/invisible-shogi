/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.logic

import invisible.shogi.logic.MoveChecker.Companion.getPossibleMoveEndingsByCoordinates
import invisible.shogi.logic.MoveChecker.Companion.isCheckmate
import invisible.shogi.model.Coordinates
import invisible.shogi.model.Piece
import invisible.shogi.model.PieceType
import invisible.shogi.model.Player
import invisible.shogi.model.move.MoveFactory.startDrop
import invisible.shogi.model.move.MoveFactory.startMove
import org.junit.jupiter.api.Assertions

fun testIsCheckmate(positionDescription: String, toPlayer: Player) {
    val position = positionFromString(positionDescription)
    position.playerToMove = toPlayer
    Assertions.assertTrue(isCheckmate(position))
}

fun checkPossibleFinishes(expectedPossibilities: String?, positionDescription: String, start: Coordinates?) {
    val position = positionFromString(positionDescription)
    val piece = position.getPieceAt(start!!)
    Assertions.assertNotNull(piece)
    position.playerToMove = piece!!.player
    val possibleMoves = getPossibleMoveEndingsByCoordinates(position, startMove(piece, start))
    Assertions.assertEquals(expectedPossibilities, possibleMovesToString(possibleMoves))
}

fun checkPossibleFinishesDrop(expectedPossibilities: String?,
                              positionDescription: String,
                              piece: Piece) {
    val position = positionFromString(positionDescription)
    position.playerToMove = piece.player
    position.setHands(mutableListOf(piece))
    val possibleMoves = getPossibleMoveEndingsByCoordinates(position, startDrop(piece))
    Assertions.assertEquals(expectedPossibilities, possibleMovesToString(possibleMoves))
}

internal fun positionFromString(string: String): Position {
    val pieces = Array(Position.BOARD_SIZE) { arrayOfNulls<Piece>(Position.BOARD_SIZE) }
    for (i in 0 until Position.BOARD_SIZE) {
        for (j in 0 until Position.BOARD_SIZE) {
            val start = (Position.BOARD_SIZE * 2 + 2) * i + 2 * j
            val chars = string.substring(start, start + 2)
            pieces[i][j] = charsToPiece(chars)
        }
    }
    return Position(pieces)
}

private fun charsToPiece(chars: String): Piece? {
    if (chars[1] == ' ') {
        return null
    } else if (chars == "^K") {
        return Piece(Player.BLACK, PieceType.KING)
    } else if (chars[1] == 'K') {
        return Piece(Player.WHITE, PieceType.KING)
    }
    val type = when (chars[1]) {
        'r' -> PieceType.ROOK
        'R' -> PieceType.PROMOTED_ROOK
        'b' -> PieceType.BISHOP
        'B' -> PieceType.PROMOTED_BISHOP
        'G' -> PieceType.GOLD
        's' -> PieceType.SILVER
        'S' -> PieceType.PROMOTED_SILVER
        'n' -> PieceType.KNIGHT
        'N' -> PieceType.PROMOTED_KNIGHT
        'l' -> PieceType.LANCE
        'L' -> PieceType.PROMOTED_LANCE
        'p' -> PieceType.PAWN
        'P' -> PieceType.PROMOTED_PAWN
        else -> error("Unexpected character: " + chars[1])
    }
    return Piece(if (chars[0] == '^') Player.BLACK else Player.WHITE, type)
}

private fun possibleMovesToString(possibleMoves: PossibleMoves): String {
    val builder = StringBuilder()
    for (i in 0 until Position.BOARD_SIZE) {
        for (j in 0 until Position.BOARD_SIZE) {
            var canWithoutPromoting = false
            var canPromoting = false
            for (move in possibleMoves[i][j]) {
                if (move.isPromoting) {
                    canPromoting = true
                } else {
                    canWithoutPromoting = true
                }
            }
            if (canPromoting && !canWithoutPromoting) {
                builder.append("|P")
            } else if (canWithoutPromoting && !canPromoting) {
                builder.append("|W")
            } else if (canPromoting) {
                builder.append("|A")
            } else {
                builder.append("| ")
            }
        }
        builder.append("|\n")
    }
    return builder.toString()
}