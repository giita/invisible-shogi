/*
 * Copyright 2021 Invisible Shogi author(s) and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invisible.shogi.logic

import invisible.shogi.engines.toUsiSfen
import invisible.shogi.model.Piece
import invisible.shogi.model.PieceType
import invisible.shogi.model.Player
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class UsiTest {
    @Test
    fun testStartingPosition() {
        val expected = "lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1"
        Assertions.assertEquals(expected, Position().toUsiSfen())
    }

    @Test
    fun testComplicatedPosition() {
        val positionString = """
            | | | | | | | | |l|
            | |l^R| | ^p| | | |
            |p| | |p^b^G| |p|p|
            |K|p|s| |p| | | | |
            ^n|n| ^p| | ^G| | |
            ^p| ^p| ^p| | ^p^p|
            | ^p^s| | | | | | |
            | ^K^s^G| | | |R| |
            ^l^n| | |P| | | ^l|
            
            """.trimIndent()
        val expected = "8l/1l+R2P3/p2pBG1pp/kps1p4/Nn1P2G2/P1P1P2PP/1PS6/1KSG3+r1/LN2+p3L w Sbgn3p 124"

        val position = positionFromString(positionString)
        position.setHands(mutableListOf(Piece(Player.WHITE, PieceType.PAWN),
                                        Piece(Player.WHITE, PieceType.GOLD),
                                        Piece(Player.WHITE, PieceType.KNIGHT),
                                        Piece(Player.WHITE, PieceType.BISHOP),
                                        Piece(Player.WHITE, PieceType.PAWN),
                                        Piece(Player.BLACK, PieceType.SILVER),
                                        Piece(Player.WHITE, PieceType.PAWN)))
        position.currentMoveNumber = 124
        Assertions.assertEquals(expected, position.toUsiSfen())
    }
}